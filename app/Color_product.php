<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Color_product extends Model
{
    use HasFactory;

    protected $fillable = [
        'color_id',
        'product_id',
        'img',
    ];

     public function getImageFullAttribute()
    {
        if (empty($this->img)){
            return 'default.png';
        } else {
            return route('get.image',$this->img);
        }
    }


    //Relationship
    public function color()
    {
        return $this->hasOne(Color::class,'id','color_id');
    }
}

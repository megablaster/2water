<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Color_product;

class Color extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'color'
    ];

    //Relation
    public function products()
    {
        return $this->hasMany(Color_product::class,'color_id','id');
    }
}

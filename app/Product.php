<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Subcategory;
use App\Color_product;
use Illuminate\Support\Str;


class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'position',
        'description',
        'status',
        'sku',
        'price',
        'label',
        'img',
        'capacity',
        'services',
        'url',
        'views',
        'microwave',
        'measure',
        'datasheet',
        'category_id',
    ];

    protected static function boot()
    {
        parent::boot();

        Product::creating(function ($model) {
            $model->position = Product::max('position') + 1;
        });
    }

    //Relationship
    public function category()
    {
        return $this->hasOne(Category::class,'id','category_id');
    }

    public function subcategory()
    {
        return $this->hasOne(Subcategory::class,'id','subcategory_id');
    }

    public function colors()
    {
        return $this->hasMany(Color_product::class,'product_id','id');
    }

    //Mutators
    public function setSkuAttribute($value)
    {
        if (empty($value)){
            $this->attributes['sku'] = Str::random(5);
        } else {
            $this->attributes['sku'] = $value;
        }
    }

    public function setUrlAttribute($url)
    {
        if (empty($url)){
            $this->attributes['url'] = Str::slug($this->name);
        } else {
            $this->attributes['url'] = $url;
        }
    }

    //Accesors
    public function getPriceFinalAttribute()
    {
        return '<div class="price">$'.$this->price.'</div>';
    }

    public function getExcerptAttribute()
    {
        return substr($this->description,0,80).'...';
    }

    public function getImageFullAttribute()
    {
        if (empty($this->img)){
            return 'default.png';
        } else {
            return route('get.image',$this->img);
        }
    }

    public function getMicrowaveTitleAttribute()
    {
        if ($this->microwave == 'on') {
            return 'Si';
        } else {
            return 'No';
        }
    }
}

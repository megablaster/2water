<?php

namespace App\Http\Controllers;

use App\Color_product;
use Illuminate\Http\Request;

class ColorProductController extends Controller
{
    public function store(Request $request)
    {
        $color = new Color_product;
        $color->color_id = $request->color_id;
        $color->product_id = $request->product_id;
        $color->save();

        return redirect()->back()->with('success','Se ha agregado el color al producto.');
    }

    public function update(Request $request)
    {
         try {

            $color = Color_product::find($request->id);
            $update = $color->update($request->except('_token'));

            if ($request->img) {

                //Upload profile
                $path = $request->file('img')->store('products');
                $color->img = $path;
                $color->save();

            }

            return redirect()->back()->with('success', 'Información de producto actualizada con éxito.');

        } catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }
    }

    public function delete($id)
    {
        $color = Color_product::find($id);

        if($color){
            $color->delete();
            return redirect()->back()->with('success', 'El color se ha eliminado.');
        }
    }
}

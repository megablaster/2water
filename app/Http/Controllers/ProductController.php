<?php

namespace App\Http\Controllers;

use App\Imports\ProductsImport;
use App\Product;
use App\User;
use App\Category;
use App\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use DataTables,Auth;

class ProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.products.index');
    }

    public function reorder(Request $request)
    {
        foreach($request->input('rows', []) as $row)
        {
            Product::find($row['id'])->update([
                'position' => $row['position']
            ]);
        }

        return response()->noContent();
    }

    public function getProductList(Request $request)
    {
        $data  = Product::orderBy('position','asc')->get();

        return Datatables::of($data)
            ->addColumn('date', function($data){
                return $data->created_at->format('d-m-Y');
            })
            ->addColumn('category', function($data){
                if ($data->category) {
                    return $data->category->name;
                } else {
                    
                }
                
            })
            ->addColumn('action', function($data){
                if (Auth::user()->can('manage_product')){
                    return '<div class="table-actions">
                                <a href="'.url('product/'.$data->id).'" ><i class="ik ik-edit-2 f-16 mr-15 text-green"></i></a>
                                <a href="'.url('product/delete/'.$data->id).'"><i class="ik ik-trash-2 f-16 text-red"></i></a>
                            </div>';
                }else{
                    return '';
                }
            })
            ->rawColumns(['date','category','action'])
            ->make(true);
    }

    public function create()
    {
        try
        {
            $categories = Category::get();
            return view('admin.products.create',compact('categories'));

        } catch (\Exception $e) {

            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }
    }

    public function store(Request $request)
    {
        // create user
        $validator = Validator::make($request->all(), [
            'name' => 'required | string ',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        }
        try
        {

            // store user information
            $product = Product::create([
                'name'     => $request->name,
                'description'    => $request->description,
                'status' => $request->status,
                'sku' => $request->sku,
                'capacity' => $request->capacity,
                'url' => Str::slug($request->name , "-"),
                'measure' => $request->measure,
                'datasheet' => $request->datasheet,
                'category_id' => $request->category_id,
                'label' => $request->label,
            ]);

            //Create position
            $product->position = $product->id;
            $product->save();

            if ($request->image) {

                //Upload profile
                $path = $request->file('image')->store('products');
                $product->img = $path;
                $product->save();

            }

            if($product){
                return redirect('products')->with('success', 'Se ha creado un producto.');
            }else{
                return redirect('products')->with('error', 'Fallo al crear el producto, intenta de nuevo.');
            }

        }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }
    }

    public function edit($id)
    {
        try
        {
            $product  = Product::find($id);
            $categories = Category::get();
            $colors = Color::get();

            if($product){

                return view('admin.products.edit', compact('product','categories','colors'));

            } else {
                return redirect('404');
            }

        } catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }
    }

    public function update(Request $request)
    {
        // Update user info
        $validator = Validator::make($request->all(), [
            'id'       => 'required',
            'name'     => 'required | string ',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        }

        try {

            $product = Product::find($request->id);
            $update = $product->update($request->except('_token','price'),);

            if ($request->image) {

                //Upload profile
                $path = $request->file('image')->store('products');
                $product->img = $path;
                $product->save();

            }

            return redirect()->back()->with('success', 'Información de producto actualizada con éxito.');

        } catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }
    }

    public function delete($id)
    {
        $product = Product::find($id);

        if($product){
            $product->delete();
            return redirect('products')->with('success', 'El producto se ha eliminado.');
        }else{
            return redirect('products')->with('error', 'Producto no encontrado.');
        }
    }

    public function import()
    {
        Excel::import(new ProductsImport, 'products.xlsx');
        return redirect('/dashboard')->with('success','All good');
    }

    public function storage($img){
        return url($img);
    }
}

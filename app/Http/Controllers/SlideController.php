<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Http\Request;
use DataTables,Auth;

class SlideController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.sliders.index');
    }

    public function getSliderList(Request $request)
    {
        $data  = Slide::get();

        return Datatables::of($data)
            ->addColumn('date', function($data){
                return $data->created_at->format('d-m-Y');
            })
            ->addColumn('image',function($data){
                return '<img src="'.$data->imagefull.'" style="width:50px;">';
            })
            ->addColumn('action', function($data){
                if (Auth::user()->can('manage_slider')){
                    return '<div class="table-actions">
                                <a href="'.url('slider/'.$data->id).'" ><i class="ik ik-edit-2 f-16 mr-15 text-green"></i></a>
                                <a href="'.url('slider/delete/'.$data->id).'"><i class="ik ik-trash-2 f-16 text-red"></i></a>
                            </div>';
                }else{
                    return '';
                }
            })
            ->rawColumns(['image','date','action'])
            ->make(true);
    }

    public function create()
    {
        try
        {
            return view('admin.sliders.create');

        } catch (\Exception $e) {

            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }
    }

    public function edit($id)
    {
        try
        {
            $slider  = Slide::find($id);

            if($slider){

                return view('admin.sliders.edit', compact('slider'));

            } else {
                return redirect('404');
            }

        } catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }
    }

    public function store(Request $request)
    {
        $slider = new Slide;
        $slider->name = $request->name;
        $slider->select = $request->select;
        $slider->save();

        //Url
        if ($request->url) {
            $slider->url = $request->url;
            $slider->save();
        }

        //Image
        if ($request->image) {
            $path = $request->file('image')->store('sliders');
            $slider->img = $path;
            $slider->save();
        }

        //Archive
        if ($request->archive) {
            $path = $request->file('archive')->store('sliders');
            $slider->archive = $path;
            $slider->save();
        }

        return redirect()->back()->with('success','Se ha agregado el rotador.');
    }

    public function update(Request $request)
    {
         try {

            $slider = Slide::find($request->id);
            $slider->name = $request->name;
            $slider->select = $request->select;
            $slider->save();

            if ($request->image) {
                //Upload profile
                $path = $request->file('image')->store('sliders');
                $slider->img = $path;
                $slider->save();
            }

            //Url
            if ($request->url) {
                $slider->url = $request->url;
                $slider->save();
            }

            //Archive
            if ($request->archive) {
                $path = $request->file('archive')->store('sliders');
                $slider->archive = $path;
                $slider->save();
            }

            return redirect()->back()->with('success', 'Información del rotador se ha actualizado con éxito.');

        } catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }
    }

    public function delete($id)
    {
        $slider = Slide::find($id);

        if($slider){
            $slider->delete();
            return redirect()->back()->with('success', 'El rotador se ha eliminado.');
        }
    }
}

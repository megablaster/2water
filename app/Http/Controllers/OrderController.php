<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use DataTables,Auth;

class OrderController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.orders.index');
    }

    public function getOrderList(Request $request)
    {
        $data  = Order::get();

        return Datatables::of($data)
            ->addColumn('date', function($data){
                return $data->created_at->format('d-m-Y');
            })
            ->rawColumns(['date'])
            ->make(true);
    }

    public function wiew($id)
    {
        $order = Order::find($id);
        $order->save();

        return view('admin.orders.show');
    }
}

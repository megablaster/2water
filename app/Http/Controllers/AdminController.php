<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;
use App\Order;
use App\Lead;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $products = Product::select('id')->get();
        $users = User::select('id')->get();
        $orders = Order::select('id')->get();
        $contacts = Lead::select('id')->get();

        return view('admin.index',compact('products','users','orders','contacts'));
    }
}

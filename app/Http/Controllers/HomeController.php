<?php

namespace App\Http\Controllers;

use App;
use App\Product;
use App\Category;
use App\Slide;
use App\System;
use App\Order as Orders;
use App\Lead;
use App\Mail\Contact;
use App\Mail\Order;
use App\Mail\Job;
use Illuminate\Http\Request;
use Mail;


class HomeController extends Controller
{
    public function index()
    {
        $sliders = Slide::get();
        return view('front.index',compact('sliders'));
    }

    public function products()
    {
        $products = Product::orderBy('position', 'asc')->paginate(12);
        return view('front.products',compact('products'));
    }

    public function category($url)
    {
        $category = Category::where('url',$url)->first();
        $products = Product::where('category_id',$category->id)->orderBy('position', 'asc')->paginate(18);
        return view('front.category',compact('category','products'));
    }

    public function business_line()
    {
        return view('front.business_line');
    }

    public function services()
    {
        return view('front.services');
    }

    public function about()
    {
        return view('front.about');
    }

    public function installation()
    {
        return view('front.installation');
    }

    public function contact()
    {
        return view('front.contact');
    }

    public function job()
    {
        return view('front.bolsa');
    }

    public function search(Request $request)
    {
        $search = $request->input('search');

        $products = Product::query()
            ->where('name', 'LIKE', "%{$search}%")
            //->orWhere('description', 'LIKE', "%{$search}%")
            ->orWhere('sku', 'LIKE', "%{$search}%")
            ->orderBy('position', 'asc')
            ->paginate(18);

        return view('front.products', compact('products','search'));
    }

    public function single($category,$url)
    {
        $product = Product::where('url',$url)->first();
        return view('front.single',compact('product'));
    }

    public function lang_change(Request $request)
    {
        App::setLocale($request->lang);
        session()->put('locale', $request->lang);
        return redirect()->back();
    }

    public function lang_changeinline(Request $request)
    {
        App::setLocale($request->lang);
        session()->put('locale', $request->lang);
        return redirect()->back();
    }

    public function send_contact(Request $request)
    {        
        $lead = new Lead;
        $lead->name = $request->name;
        $lead->company = $request->company;
        $lead->email = $request->email;
        $lead->phone = $request->phone;
        $lead->comments = $request->comments;
        $lead->select = 'job';
        $lead->save();

        //Send contact
        Mail::to('codingear@gmail.com')->send(new Contact($lead));

        return redirect()->back()->with('success','Se ha enviado la solicitud.');
    }

    public function send_job(Request $request)
    {
        $lead = new Lead;
        $lead->name = $request->name;
        $lead->email = $request->email;
        $lead->phone = $request->phone;
        $lead->comments = $request->comments;
        $lead->select = 'job';
        $lead->save();

        if ($request->archive) {
            //Upload archive
            $path = $request->file('archive')->store('contact');
            $lead->archive = $path;
            $lead->save();
        }

        //Send Job
        Mail::to('codingear@gmail.com')->send(new Job($lead));

        return redirect()->back()->with('success','Se ha enviado la solicitud.');

    }

    public function send_product(Request $request)
    {
        // dd($request->all());
        $order = new Orders;
        $order->name = $request->name;
        $order->product = $request->product;
        $order->company = $request->company;
        $order->email = $request->email;
        $order->phone = $request->phone;
        $order->quantity = $request->quantity;
        $order->comments = $request->comments;
        $order->save();

        if ($request->color) {
            $order->color = $request->color;
            $order->save();
        }

        //Send product
        Mail::to('codingear@gmail.com')->send(new Order($order));

        return redirect()->back()->with('success','Se ha enviado la solicitud.');

    }

    public function clearCache()
    {
        \Artisan::call('cache:clear');
        return view('clear-cache');
    }

    public function catalog()
    {
        $system = System::find(1);
        return view('front.catalog',compact('system'));
    }


}

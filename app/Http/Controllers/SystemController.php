<?php

namespace App\Http\Controllers;

use App\System;
use Illuminate\Http\Request;

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function edit($id)
    {
        $system = System::find(1);
        return view('admin.systems.edit',compact('system'));
    }

    public function update(Request $request)
    {
        $system = System::find(1);

        if ($request->archive) {
            //Upload catalog
            $path = $request->file('archive')->store('systems');
            $system->catalog = $path;
            $system->save();
        }

        return redirect()->back()->with('success','Se ha actualizado el catálogo.');
    }
}

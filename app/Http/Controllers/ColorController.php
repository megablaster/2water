<?php

namespace App\Http\Controllers;

use App\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DataTables,Auth;

class ColorController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.colors.index');
    }

    public function getColorList(Request $request)
    {
        $data  = Color::get();

        return Datatables::of($data)
            ->addColumn('date', function($data){
                if ($data->created_at) {
                    return $data->created_at->format('d-m-Y');
                } else {
                }
            })
            ->addColumn('products', function($data){
                return $data->products->count();
            })
            ->addColumn('colors', function($data){
                return '<div class="color" style="width: 100%;height: 23px;display: block;margin: 0 auto;background-color:'.$data->color.';"></div>';
            })
            ->addColumn('action', function($data){
                if (Auth::user()->can('manage_product')){
                    return '<div class="table-actions">
                                <a href="'.url('color/'.$data->id).'" ><i class="ik ik-edit-2 f-16 mr-15 text-green"></i></a>
                                <a href="'.url('color/deleted/'.$data->id).'"><i class="ik ik-trash-2 f-16 text-red"></i></a>
                            </div>';
                }else{
                    return '';
                }
            })
            ->rawColumns(['date','colors','products','action'])
            ->make(true);
    }

    public function create()
    {
        try
        {
            return view('admin.colors.create');

        } catch (\Exception $e) {

            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }
    }

    public function store(Request $request)
    {
        // Create color
        $validator = Validator::make($request->all(), [
            'name' => 'required | string ',
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        } 
            try
        {
            // store user information
            $color = new Color;
            $color->name = $request->name;
            $color->color = $request->color;
            $color->save();

            if($color){
                return redirect('colors')->with('success', 'Se ha creado el color.');
            }else{
                return redirect('colors')->with('error', 'Fallo al crear el color, intenta de nuevo.');
            }

        }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }
    }

    public function edit($id)
    {
        try
        {
            $color  = Color::find($id);

            if($color){

                return view('admin.colors.edit', compact('color'));

            } else {
                return redirect('404');
            }

        } catch (\Exception $e) {

            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }
    }

    public function update(Request $request)
    {
        // Update user info
        $validator = Validator::make($request->all(), [
            'name'       => 'required',
            'color'     => 'required | string ',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        }

        try {

            $color = Color::find($request->id);
            $color->update($request->except('_token'));

            return redirect()->back()->with('success', 'Información del color actualizada con éxito.');

        } catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }
    }

    public function delete($id)
    {
        $color = Color::find($id);

        //Check products
        if ($color->products->count() > 0) {

            return redirect('colors')->with('error', 'No se puede eliminar el color por que esta asignado a multiples productos.');

        } else {

            $color->delete();
            return redirect('colors')->with('success', 'El producto se ha eliminado.');

        }
    }
}

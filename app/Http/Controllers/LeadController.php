<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;
use DataTables,Auth;

class LeadController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.leads.index');
    }

    public function getLeadList(Request $request)
    {
        $data  = Lead::get();



        return Datatables::of($data)
            ->addColumn('date', function($data){
                return $data->created_at->format('d-m-Y');
            })
            ->addColumn('selects', function($data){
                return $data->selects;
            })
            ->addColumn('archive', function($data){
                if ($data->archive) {
                    $route = route('get.image',$data->archive);    
                    return '<a href="'.$route.'" target="_blank" class="btn btn-sm btn-warning">Ver CV</a>';
                } else {

                }
                
            })
            ->rawColumns(['date','archive','selects'])
            ->make(true);
    }

    public function wiew($id)
    {
        $lead = Lead::find($id);
        $lead->save();

        return view('admin.leads.show');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'url',
        'img',
        'select',
        'archive'
    ];

     public function getImageFullAttribute()
    {
        if (empty($this->img)){
            return 'default.png';
        } else {
            return route('get.image',$this->img);
        }
    }
}

<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use App\Models\Consultation;
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'img',
        'last_name',
        'last_name_mother',
        'phone',
        'address',
        'state',
        'municipality',
        'status',
        'birthday',
        'billing',
        'gender',
        'cp',
        'nationality',
        'occupation',
        'level_of_education'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function get_roles(){
        $roles = [];
        foreach ($this->getRoleNames() as $key => $role) {
            $roles[$key] = $role;
        }

        return $roles;
    }

    public function consulting()
    {
        return $this->hasMany(Consultation::class,'user_id','id')->whereDate('schedule','>=', Carbon::now());
    }

    public function getAgeAttribute()
    {
        $date = Carbon::parse($this->birthday);
        return $date->diff(Carbon::now())->format('%y años, %m meses y %d días');
    }
}

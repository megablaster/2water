<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'company',
        'email',
        'phone',
        'select',
        'archive',
        'comments'
    ];

    public function getSelectsAttribute()
    {
        if ($this->select == 'job') {
            return '<span class="badge badge-primary">Bolsa de trabajo</span>';
        } else {
            return '<span class="badge badge-success">Contacto</span>';
        }
        
    }
}

@extends('layouts.main') 
@section('title', $user->name)
@section('content')
    <!-- push external head elements to head -->
    @push('head')
        <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    @endpush

    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-user-plus bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Editar usuario')}}</h5>
                            <span>{{ __('Crear nueva usuario, asignar roles y permisos.')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('User')}}</a>
                            </li>
                            <li class="breadcrumb-item">
                                {{ clean($user->name, 'titles')}}
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card" id="user-edit">
                    <div class="card-body">
                        <form class="forms-sample" method="POST" action="{{ url('user/update') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Información básica</h4>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <div style="height: 160px;margin-bottom:10px;background-size:cover;background-image: url('{{route('get.image',$user->img)}}');"></div>
                                                <input type="file" name="image" class="form-control">
                                            </div> 
                                        </div>
                                        <div class="col-lg-2">
                                            <!-- Assign role & view role permisions -->
                                            <div class="form-group">
                                                <label for="role">{{ __('Asignar rol')}}<span class="text-red">*</span></label>
                                                {!! Form::select('role', $roles, $user_role->id??'' ,[ 'class'=>'form-control select2', 'placeholder' => 'Selecciona el rol','id'=> 'role', 'required'=>'required']) !!}
                                            </div>
                                            <div class="form-group" style="display: none;">
                                                <div id="permission" class="form-group">
                                                    @foreach($user->getAllPermissions() as $key => $permission) 
                                                    <span class="badge badge-dark m-1">
                                                        <!-- clean unescaped data is to avoid potential XSS risk -->
                                                        {{ clean($permission->name, 'titles')}}
                                                    </span>
                                                    @endforeach
                                                </div>
                                                <input type="hidden" id="token" name="token" value="{{ csrf_token() }}">
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="name">{{ __('Nombre')}}<span class="text-red">*</span></label>
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$user->name}}" required>
                                                <div class="help-block with-errors"></div>
                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>  
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="last_name">{{ __('Apellido paterno')}}<span class="text-red">*</span></label>
                                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{$user->last_name}}" required>
                                                <div class="help-block with-errors"></div>
                                                @error('last_name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="last_name_mother">{{ __('Apellido materno')}}<span class="text-red">*</span></label>
                                                <input id="last_name_mother" type="text" class="form-control @error('last_name_mother') is-invalid @enderror" name="last_name_mother" value="{{$user->last_name_mother}}">
                                                <div class="help-block with-errors"></div>
                                                @error('last_name_mother')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="email">{{ __('Correo electrónico')}}<span class="text-red">*</span></label>
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email}}">
                                                <div class="help-block with-errors" ></div>
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="phone">{{ __('Teléfono')}}<span class="text-red">*</span></label>
                                                <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{$user->phone}}">
                                                <div class="help-block with-errors" ></div>
                                                @error('phone')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="birthday">{{ __('Fecha de nacimiento')}}<span class="text-red">*</span></label>
                                                <input id="birthday" type="date" class="form-control @error('birthday') is-invalid @enderror" name="birthday" value="{{$user->birthday}}">
                                                <div class="help-block with-errors" ></div>
                                                @error('birthday')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="state">{{ __('Estado')}}<span class="text-red">*</span></label>
                                                <input id="state" type="text" class="form-control @error('state') is-invalid @enderror" name="state" value="{{$user->state}}">
                                                <div class="help-block with-errors" ></div>
                                                @error('state')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="address">{{ __('Dirección')}}<span class="text-red">*</span></label>
                                                <input id="address" type="text" class="form-control @error('state') is-invalid @enderror" name="address" value="{{$user->address}}">
                                                <div class="help-block with-errors" ></div>
                                                @error('address')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <h4>Contraseña</h4>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="password">{{ __('Contraseña')}}<span class="text-red">*</span></label>
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">
                                                <div class="help-block with-errors"></div>
                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="password-confirm">{{ __('Confirmar contraseña')}}<span class="text-red">*</span></label>
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary form-control-right">{{ __('Actualizar')}}</button>
                                    </div>
                                </div>
                            </div>
                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- push external js -->
    @push('script') 
        <script src="{{ asset('plugins/select2/dist/js/select2.min.js') }}"></script>
        <!--get role wise permissiom ajax script-->
        <script src="{{ asset('js/get-role.js') }}"></script>
    @endpush
@endsection

@extends('layouts.main')
@section('title', 'Agregar rotador')

@section('content')
    <!-- push external head elements to head -->
    @push('head')
        <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
        <style>
            #archive {
                display: none;
            }
        </style>
    @endpush

    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-user-plus bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Agregar rotador')}}</h5>
                            <span>{{ __('Crear nueva rotador.')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Agregar rotador')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-6">
                <div class="card ">
                    <div class="card-header">
                        <h3>{{ __('Agregar rotador')}}</h3>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample" method="POST" action="{{ url('slider/create') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label for="name">{{ __('Imagen de producto')}}<span class="text-red">*</span></label>
                                                <input type="file" name="image" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="name">{{ __('Nombre')}}<span class="text-red">*</span></label>
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required>
                                                <div class="help-block with-errors"></div>
                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="form-group">
                                                <label for="name">{{ __('Selecciona url o archivo')}}<span class="text-red">*</span></label>
                                                <select name="select" id="select" class="form-control">
                                                    <option value="url">Url</option>
                                                    <option value="empty">Ninguno</option>
                                                    <option value="archive">Archivo pdf</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 view" id="url">
                                            <div class="form-group">
                                                <label for="url">{{ __('Url')}}<span class="text-red">*</span></label>
                                                <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url">
                                                <div class="help-block with-errors"></div>
                                                @error('url')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12 view" id="archive">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for="name">{{ __('Archivo pdf')}}<span class="text-red">*</span></label>
                                                    <input type="file" name="archive" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary form-control-right">{{ __('Agregar')}}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- push external js -->
    @push('script')
        <script src="{{ asset('plugins/select2/dist/js/select2.min.js') }}"></script>
        <script>
            $('#select').on('change',function(){
                var select = $(this).val();

                $('.view').hide();
                $('#'+ select).show();
            });
        </script>
    @endpush
@endsection

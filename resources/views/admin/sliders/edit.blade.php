@extends('layouts.main')
@section('title', $slider->name)
@section('content')
    <!-- push external head elements to head -->
    @push('head')
        <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    @endpush

    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-user-plus bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Editar rotador')}}</h5>
                            <span>{{ __('Crear nuevo rotador.')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Rotador')}}</a>
                            </li>
                            <li class="breadcrumb-item">
                                {{ clean($slider->name, 'titles')}}
                            </li>

                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card" id="user-edit">
                    <div class="card-body">
                        <form class="forms-sample" method="POST" action="{{ url('slider/update') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$slider->id}}">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Información básica</h4>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <div class="img" style="height: 250px;background-size: cover;background-position: center;background-image: url('{{route('get.image',$slider->img)}}');"></div>
                                                <input type="file" name="image" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="name">{{ __('Nombre')}}<span class="text-red">*</span></label>
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$slider->name}}" required>
                                                <div class="help-block with-errors"></div>
                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <div class="form-group">
                                                <label for="name">{{ __('Selecciona url o archivo')}}<span class="text-red">*</span></label>
                                                <select name="select" id="select" class="form-control">
                                                    <option value="empty" @if ($slider->select == 'empty') selected @endif>Ninguno</option>
                                                    <option value="url" @if ($slider->select == 'url') selected @endif>Url</option>
                                                    <option value="archive" @if ($slider->select == 'archive') selected @endif>Archivo pdf</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 view" id="url">
                                            <div class="form-group">
                                                <label for="url">{{ __('Url')}}<span class="text-red">*</span></label>
                                                <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" value="{{$slider->url}}">
                                                <div class="help-block with-errors"></div>
                                                @error('url')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12 view" id="archive">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for="name">{{ __('Archivo pdf')}}<span class="text-red">*</span></label>
                                                    <input type="file" name="slider" class="form-control">
                                                    <a href="@{{route('get.image',$slider->archive)}}" target="_blank" class="btn btn-primary">Ver archivo</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary form-control-right">{{ __('Actualizar rotador')}}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- push external js -->
    @push('script')
        <script>
            $(document).ready(function(){

                var active = '<?php echo $slider->select ?>';

                if (active == 'url') {
                    $('#archive').hide();
                    $('#url').show();
                } else if(active == 'archive') {
                    $('#archive').show();
                    $('#url').hide();
                } else {
                    $('#archive').hide();
                    $('#url').hide();
                }

                $('#select').on('change',function(){
                    var select = $(this).val();
                    $('.view').hide();
                    $('#'+ select).show();
                });

            });
        </script>
    @endpush
@endsection

@extends('layouts.main')
@section('title', $product->name)
@section('content')
    <!-- push external head elements to head -->
    @push('head')
        <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    @endpush

    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-user-plus bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Editar producto')}}</h5>
                            <span>{{ __('Crear nuevo producto.')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Producto')}}</a>
                            </li>
                            <li class="breadcrumb-item">
                                {{ clean($product->name, 'titles')}}
                            </li>

                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card" id="user-edit">
                    <div class="card-body">
                        <form class="forms-sample" method="POST" action="{{ url('product/update') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{$product->id}}">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Información básica</h4>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <div style="height: 160px;margin-bottom:10px;background-size:cover;background-image: url('{{route('get.image',$product->img)}}');"></div>
                                                <input type="file" name="image" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="name">{{ __('Nombre')}}<span class="text-red">*</span></label>
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$product->name}}" required>
                                                <div class="help-block with-errors"></div>
                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="url">{{ __('Url')}}<span class="text-red">*</span></label>
                                                <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" value="{{$product->url}}" required>
                                                <div class="help-block with-errors"></div>
                                                @error('url')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="description">{{ __('Descripción')}}<span class="text-red">*</span></label>
                                                <textarea id="description" name="description" class="form-control @error('description') is-invalid @enderror" rows="6">{{$product->description}}</textarea>
                                                <div class="help-block with-errors"></div>
                                                @error('description')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="microwave">{{ __('Resistencia a microondas')}}<span class="text-red">*</span></label>
                                                <select name="microwave" id="microwave" class="form-control">
                                                    <option value="on" {{($product->microwave == 'on')?'selected':''}}>Si</option>
                                                    <option value="off" {{($product->microwave == 'off')?'selected':''}}>No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="measure">{{ __('Medidas')}}<span class="text-red">*</span></label>
                                                <input type="text" class="form-control" name="measure" value="{{$product->measure}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="category_id">{{ __('Categoría')}}<span class="text-red">*</span></label>
                                                <select name="category_id" id="category_id" class="form-control">
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}" {{($product->category_id == $category->id)?'selected':''}}>{{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="services">{{ __('Servicios')}}<span class="text-red">*</span></label>
                                                <input id="services" type="text" class="form-control @error('services') is-invalid @enderror" name="services" value="{{$product->services}}">
                                                <div class="help-block with-errors"></div>
                                                @error('services')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="sku">{{ __('Sku')}}<span class="text-red">*</span></label>
                                                <input id="sku" type="text" class="form-control @error('sku') is-invalid @enderror" name="sku" value="{{$product->sku}}">
                                                <div class="help-block with-errors"></div>
                                                @error('sku')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="price">{{ __('Precio')}}<span class="text-red">*</span></label>
                                                <input id="price" type="text" class="form-control @error('price') is-invalid @enderror" name="price" value="{{$product->price}}">
                                                <div class="help-block with-errors"></div>
                                                @error('price')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="capacity">{{ __('Capacidad')}}<span class="text-red">*</span></label>
                                                <input id="capacity" type="text" class="form-control @error('capacity') is-invalid @enderror" name="capacity" value="{{$product->capacity}}">
                                                <div class="help-block with-errors"></div>
                                                @error('capacity')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary form-control-right">{{ __('Actualizar producto')}}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @include('include.message')
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <h4>Agregar colores</h4>
                        <hr>
                        <form action="{{url('color/create')}}" method="post">
                            @csrf
                            <input type="hidden" name="product_id" value="{{$product->id}}">
                            <div class="form-group">
                                <select class="form-control" name="color_id">
                                    @foreach($colors as $color)
                                        <option value="{{$color->id}}">{{$color->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success">Agregar color</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- end message area-->
            @foreach($product->colors as $color)

                <div class="col-md-3">
                    <div class="card" id="user-edit">
                        <div class="card-body">

                            <form class="forms-sample" method="POST" action="{{ url('color/update') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$color->id}}">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4>{{$color->color->name}}</h4><hr>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div style="height: 160px;margin-bottom:10px;background-size:cover;background-image: url('{{route('get.image',$color->img)}}');"></div>
                                                    <input type="file" name="img" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{url('color/delete/'.$color->id)}}" class="btn btn-danger btn-sm">Eliminar</a>
                                <button type="submit" class="btn btn-primary">Actualizar foto</button>
                            </form>

                        </div>
                    </div>
                </div>

            @endforeach
        </div>
    </div>
    <!-- push external js -->
    @push('script')
        <script src="{{ asset('plugins/select2/dist/js/select2.min.js') }}"></script>
    @endpush
@endsection

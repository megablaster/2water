@extends('layouts.main')
@section('title', 'Productos')
@section('content')

    <!-- push external head elements to head -->
    @push('head')
        <link rel="stylesheet" href="{{ asset('plugins/DataTables/datatables.min.css') }}">
        <link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.8/css/rowReorder.dataTables.min.css">
    @endpush

    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-users bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Productos')}}</h5>
                            <span>{{ __('Lista de productos')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Productos')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card p-3">
                    <div class="card-header"><h3>{{ __('Productos')}}</h3></div>
                    <div class="card-body">
                        <table id="product_table" class="table">
                            <thead>
                                <tr>
                                    <th>{{ __('Nombre')}}</th>
                                    <th>{{ __('Categoría')}}</th>
                                    <th>{{ __('Orden')}}</th>
                                    <th>{{ __('Sku')}}</th>
                                    <th>{{ __('Capacidad')}}</th>
                                    <th>{{ __('Fecha de creación')}}</th>
                                    <th>{{ __('Acciones')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- push external js -->
    @push('script')
    <script src="{{ asset('plugins/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.min.js') }}"></script>
    <script src="https://cdn.datatables.net/rowreorder/1.2.8/js/dataTables.rowReorder.min.js"></script>
    <!--server side users table script-->
    {{-- <script src="{{ asset('js/custom.js') }}"></script> --}}
    <script>
        $(function () {
          let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

          let dtOverrideGlobals = {
            buttons: dtButtons,
            processing: true,
            serverSide: true,
            retrieve: true,
            aaSorting: [],
            ajax: "{{ url('product/get-list') }}",
            columns: [
                {data:'name', name: 'name'},
                {data:'category', name: 'category'},
                {data: 'position', name: 'position', visible: true, searchable: false },
                {data:'sku', name: 'sku'},
                {data:'capacity', name: 'capacity'},
                {data:'date', name: 'date'},
                {data:'action', name: 'action'}
            ],
            order: [[ 2, 'asc' ]],
            pageLength: 200,
            aLengthMenu: [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "Todos"]
            ],
            rowReorder: {
                selector: 'tr td:not(:first-of-type,:last-of-type)',
                dataSrc: 'position'
            },
          };

          let datatable = $('#product_table').DataTable(dtOverrideGlobals);
            datatable.on('row-reorder', function (e, details) {
                if(details.length) {
                    let rows = [];
                    details.forEach(element => {
                        rows.push({
                            id: datatable.row(element.node).data().id,
                            position: element.newData
                        });
                    });

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        method: 'POST',
                        url: "{{ route('product.reorder') }}",
                        data: { rows }
                    }).done(function () { datatable.ajax.reload() });
                }
            });
        });
    </script>
    @endpush
@endsection

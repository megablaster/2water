@component('mail::message')
# Hola, administrador

<p>Se ha recibido una solicitud de bolsa de trabajo.</p>

<strong>Nombre:</strong> {{$lead->name}}<br>
<strong>Compañia:</strong> {{$lead->company}}<br>
<strong>Correo:</strong> {{$lead->email}}<br>
<strong>Teléfono:</strong> {{$lead->phone}}<br>
<strong>Tipo de consulta:</strong> {!!$lead->selects!!}<br>
<strong>Comentarios:</strong> {{$lead->comments}}<br>

<?php
	$urlFull = route('get.image',$lead->archive);
?>
@component('mail::button', ['url' => $urlFull])
	Descargar CV
@endcomponent

@endcomponent

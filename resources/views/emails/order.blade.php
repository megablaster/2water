@component('mail::message')
# Hola, administrador

<p>Se ha recibido una solicitud de producto.</p>

<strong>Nombre:</strong> {{$order->name}}<br>
<strong>Producto:</strong> {{$order->product}}<br>
<strong>Correo:</strong> {{$order->email}}<br>
<strong>Teléfono:</strong> {{$order->phone}}<br>
<strong>Color:</strong> {{$order->color}}<br>
<strong>Cantidad:</strong> {{$order->quantity}}<br>
<strong>Comentarios:</strong> {{$order->comments}}

@endcomponent

@extends('layouts.front')
@section('title', 'Inicio')

@push('head')
    <link rel="stylesheet" href="{{ asset('plugins/owl.carousel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/owl.carousel/dist/assets/owl.theme.default.min.css') }}">
@endpush

@section('content')
    <section id="single-product">
        <section id="header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="breadcrumb-product">
                            <li><a href="{{route('front.index')}}">Inicio</a><span>/</span></li>
                            <li><a href="{{route('front.category',$product->category->url)}}">{{$product->category->name}}</a><span>/</span></li>
                            <li><a href="{{route('front.single',[Str::slug($product->category->url),$product->url])}}">{{$product->name}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <div class="container" id="info-product">
            <div class="row">
                <div class="col-xl-12">
                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            {!! Session::get('success') !!}
                        </div>
                    @endif
                </div>
                <div class="col-xl-5">

                    <div class="owl-carousel owl-single">
                        @if ($product->colors->count() > 0)
                            @foreach($product->colors as $color)
                                <div class="item">
                                    <div class="img" style="background-image: url('{{$color->ImageFull}}')"></div>  
                                </div>
                            @endforeach
                        @else
                            <div class="item">
                                <div class="img" style="background-image: url('{{$product->ImageFull}}')"></div>  
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-xl-4 p-relative">
                    <div class="info">
                        <h1>{{$product->name}}</h1>
                        <div class="sku">{{$product->sku}}</div>
                        <p style="margin-bottom: 10px;">{{$product->description}}</p>
                        @if($product->colors->count() > 0)
                            <h5>Colores:</h5>
                            <?php
                                $count = 0;
                            ?>
                            <ul class="colors">
                                @foreach($product->colors as $color)
                                    <li>
                                        <div class="circle click-circle" data-id="<?php echo $count;?>" style="background-color:{{$color->color->color}}"></div>
                                    </li>

                                    <?php $count = $count + 1?>
                                @endforeach
                            </ul>
                        @endif
                        <h4>{{$product->label}}</h4>
                        <hr>
                        <img src="{{asset('assets/img/fda.png')}}" alt="FDA" class="img-fluid fda">
                    </div>
                </div>
                <div class="col-xl-3">
                     <h4>{{__('main.information')}}</h4>
                     <form id="form" method="post" action="{{route('send.product')}}">
                        @csrf
                        <input type="hidden" name="product" class="form-control" value="{{$product->name}}" readonly>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="{{__('main.form.name')}}" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="company" class="form-control" placeholder="{{__('main.form.company')}}" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="{{__('main.form.email')}}" required>
                        </div>
                        <div class="form-group">
                            <input type="tel" name="phone" class="form-control" placeholder="{{__('main.form.phone')}}" required>
                        </div>
                        @if ($product->colors->count() > 0)
                        <div class="form-group">
                            <select name="color" class="form-control">
                                <option value="">{{__('main.form.color')}}</option>
                                @foreach($product->colors as $color)
                                    <option>{{$color->color->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <div class="form-group">
                            <input type="text" name="quantity" class="form-control" placeholder="{{__('main.form.quantity')}}" required>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="comments" placeholder="{{__('main.form.comments')}}"></textarea>
                        </div>
                        <button type="submit" class="btn btn-submit">{{__('main.submit')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section id="additional-info">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <h2>{{__('main.info_aditional')}}</h2>
                    <table class="table table-sm">
                        <tbody>
                            <tr style="border-top:1px solid black;border-left:1px solid black;">
                                <td>Sku</td>
                                <td>{{$product->sku}}</td>
                            </tr>
                            <tr style="border-left:1px solid black;">
                                <td>{{__('main.info.name')}}</td>
                                <td>{{$product->name}}</td>
                            </tr>
                            <tr style="border-left:1px solid black;">
                                <td>{{__('main.info.capacity')}}</td>
                                <td>{{$product->capacity}}</td>
                            </tr>
                            @if ($product->colors->count() > 0)
                                <tr style="border-left:1px solid black;">
                                    <td>{{__('main.info.color')}}</td>
                                    <td>
                                        @foreach($product->colors as $color)
                                            {{$color->color->name}},
                                        @endforeach
                                    </td>
                                </tr>
                            @endif
                            <tr style="border-left:1px solid black;border-bottom: 1px solid black;">
                                <td>{{__('main.info.measure')}}</td>
                                <td>{{$product->measure}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection

<!-- push external js -->
@push('script')
    <script src="{{ asset('plugins/owl.carousel/dist/owl.carousel.min.js') }}"></script>
    <script>
        var owl = $('.owl-single');
        owl.owlCarousel({
            loop:true,
            margin:0,
            dots:false,
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });

        $('.click-circle').on('click', function(){
            $('.click-circle').removeClass('active');
            $(this).addClass('active');
            var id = $(this).data('id');
            owl.trigger('to.owl.carousel', [id,300]);
        });
    </script>
@endpush

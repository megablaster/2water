@extends('layouts.front')
@section('title', 'Catálogo digital')

@push('head')
    <link rel="stylesheet" href="{{asset('/script/flipbook/flipbook.style.css')}}">
@endpush

@section('content')
    <div id="flipbook"></div>
@endsection

@push('script')
    <script src="{{asset('/script/flipbook/flipbook.min.js')}}"></script>
    <script>
        $(document).ready(function() {

            $("#flipbook").flipBook({
                pdfUrl: '{{route('get.image',$system->catalog)}}',
                zoomMin:0.9,
                skin:"dark",
                viewMode:"swipe",
                singlePageMode:true
            });

        });
    </script>
@endpush

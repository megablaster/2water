@extends('layouts.front')
@section('title', 'Líneas de negocio')

@push('head')
@endpush

@section('content')
    <section id="business-line">
        <section id="header" style="background-image:url('{{asset('assets/img/home-info.jpeg')}}')">
            <h1>{{__('business.title')}}</h1>
        </section>
        <section id="info1">
            <div class="container" id="promocionales">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <h2>{{__('business.promotion.title')}}</h2>
                        <p>{{__('business.promotion.text1')}}</p>
                        <p>{{__('business.promotion.text2')}}</p>
                        <p>{{__('business.promotion.text3')}}</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="info2">
            <div class="container" id="retail-y-hogar">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <h2>{{__('business.retail.title')}}</h2>
                        <p>{{__('business.retail.text1')}}</p>
                        <p>{{__('business.retail.text2')}}</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="info3" style="display: none;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                       <h2>{{__('business.envase.title')}}</h2>
                        <p>{{__('business.envase.text1')}}</p>
                        <div id="exportacion"></div>
                        <p>{{__('business.envase.text2')}}</p>
                        <p>{{__('business.envase.text3')}}</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="info4">
            <div class="container" id="exportaciones">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <h2>{{__('business.export.title')}}</h2>
                        <p>{{__('business.export.text1')}}</p>
                        <p>{{__('business.export.text2')}}</p>
                        <div class="row">
                            <div class="col-md-4">
                                <ul>
                                    <li>Argentina</li>
                                    <li>Chile</li>
                                    <li>Estados Unidos</li>
                                    <li>Colombia</li>
                                    <li>Perú</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <li>Guatemala</li>
                                    <li>Panamá</li>
                                    <li>Costa Rica</li>
                                    <li>Puerto Rico</li>
                                    <li>El Salvador</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <li>Honduras</li>
                                    <li>Nicaragua</li>
                                    <li>Bahamas</li>
                                    <li>Jamaica</li>
                                    <li>Trinidad y Tobago</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="info5">
            <div class="container" id="certificaciones">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <h2>{{__('business.certificate.title')}}</h2>
                        <p>{{__('business.certificate.text1')}}</p>
                        <img src="{{asset('assets/img/business/logos_1.png')}}" class="img-fluid">
                        <p>{{__('business.certificate.text2')}}</p>
                        <ul>
                            <li>{{__('business.certificate.text3')}}</li>
                            <li>{{__('business.certificate.text4')}}</li>
                            <li>{{__('business.certificate.text5')}}</li>
                            <li>{{__('business.certificate.text6')}}</li>
                            <li>{{__('business.certificate.text7')}}</li>
                        </ul>
                        <img src="{{asset('assets/img/business/logos_2.png')}}" class="img-fluid">
                        <h4 class="text-center">{{__('business.certificate.text8')}}</h4>
                        <img src="{{asset('assets/img/business/logos_3.png')}}" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>
        <section id="info6" style="display: none;">
            <div class="container" id="maquilas">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <h2>{{__('business.maquila.title')}}</h2>
                        <p>{{__('business.maquila.text1')}}</p>
                        <p>{{__('business.maquila.text2')}}</p>
                        <p>{{__('business.maquila.text3')}}</p>
                        <p>{{__('business.maquila.text4')}}</p>
                        <div class="row">
                            <div class="col-xl-4">
                                <ul>
                                    <li>{{__('business.maquila.text5')}}</li>
                                    <li>{{__('business.maquila.text6')}}</li>
                                    <li>{{__('business.maquila.text7')}}</li>
                                    <li>{{__('business.maquila.text8')}}</li>
                                </ul>
                            </div>
                            <div class="col-xl-4">
                                <ul>
                                    <li>{{__('business.maquila.text9')}}</li>
                                    <li>{{__('business.maquila.text10')}}</li>
                                    <li>{{__('business.maquila.text11')}}</li>
                                    <li>{{__('business.maquila.text12')}}</li>
                                </ul>
                            </div>
                            <div class="col-xl-4">
                                <ul>
                                    <li>{{__('business.maquila.text13')}}</li>
                                    <li>{{__('business.maquila.text14')}}</li>
                                </ul>
                            </div>
                        </div>
                        <img src="{{asset('assets/img/business/maquinaria.png')}}">
                        <h4>{{__('business.maquila.text15')}}</h4>
                        <p>{{__('business.maquila.text16')}}</p>
                        <h4>{{__('business.maquila.text17')}}</h4>
                        <p>{{__('business.maquila.text18')}}</p>
                        <h4>{{__('business.maquila.text19')}}</h4>
                        <p>{{__('business.maquila.text20')}}</p>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

<!-- push external js -->
@push('script')
@endpush

@extends('layouts.front')
@section('title', 'Instalaciones')

@push('head')
@endpush

@section('content')
    <section id="services">
        <section id="header" style="background-image:url('{{asset('assets/img/home-info.jpeg')}}')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="text-center">{{__('installation.title')}}</h1>
                    </div>
                </div>
            </div>
        </section>
        <section id="info1">
            <div class="container-fluid">
                <div class="row" id="maquilas">
                    <div class="col-lg-4 offset-lg-1">
                        <div class="text">
                            <h3>{{__('installation.plant_title')}}</h3>
                            {!!__('installation.plant_text')!!}
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-1 p-0">
                        <div class="img" style="background-image: url('{{asset('assets/img/04-min.jpg')}}')"></div>
                    </div>
                </div>
                <div class="row" id="desarrollo-de-productos">
                    <div class="col-lg-6 p-0 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2">
                        <div class="img" style="background-image: url('{{asset('assets/img/06-min.jpg')}}')"></div>
                    </div>
                    <div class="col-lg-4 offset-lg-1 bg-black order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1">
                        <div class="text">
                            <h3>{{__('installation.taller_title')}}</h3>
                            {!!__('installation.taller_text')!!}
                        </div>
                    </div>
                </div>
                <div class="row" id="fabricacion-de-moldes">
                    <div class="col-lg-4 offset-lg-1 bg-black">
                        <div class="text">
                            <h3>{{__('installation.laboratory_title')}}</h3>
                            {!!__('installation.laboratory_text')!!}
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-1 p-0">
                        <div class="img" style="background-image: url('{{asset('img/laboratorio_calidad.jpg')}}')"></div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

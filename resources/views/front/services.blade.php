@extends('layouts.front')
@section('title', 'Servicios')

@push('head')
    <style>
        #navbar {
            position: relative!important;
        }
    </style>
@endpush

@section('content')
    <section id="services">
        <section id="header" style="background-image:url('{{asset('assets/img/home-info.jpeg')}}')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="text-center">{{__('services.title')}}</h1>
                    </div>
                </div>
            </div>
        </section>
        <section id="info1">
            <div class="container-fluid">
                <div class="row" id="maquilas">
                    <div class="col-lg-4 offset-lg-1">
                        <div class="text">
                            <h3>{{__('services.title_maquila')}}</h3>
                            {!!__('services.text_maquila')!!}
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-1 p-0">
                        <div class="img" style="background-image: url('{{asset('assets/img/01-min.jpg')}}')"></div>
                    </div>
                </div>
                <div class="row" id="desarrollo-de-productos">
                    <div class="col-lg-6 p-0 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2">
                        <div class="img" style="background-image: url('{{asset('assets/img/desarrollo.jpg')}}')"></div>
                    </div>
                    <div class="col-lg-4 offset-lg-1 bg-black order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1">
                        <div class="text">
                            <h3>{{__('services.title_product')}}</h3>
                            {!!__('services.text_product')!!}
                        </div>
                    </div>
                </div>
                <div class="row" id="fabricacion-de-moldes">
                    <div class="col-lg-4 offset-lg-1 bg-black">
                        <div class="text">
                            <h3>{{__('services.title_moldes')}}</h3>
                            {!!__('services.text_moldes')!!}
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-1 p-0">
                        <div class="img" style="background-image: url('{{asset('assets/img/fabricacion.jpg')}}')"></div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

@extends('layouts.front')
@section('title', 'Nosotros')

@push('head')
@endpush

@section('content')
    
    <section id="about">
        <section id="header" style="background-image:url('{{asset('assets/img/home-info.jpeg')}}')">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="text-center">{{__('about.title')}}</h1>
                    </div>
                </div>
            </div>
        </section>
        <section id="info">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <h1>{{__('about.title_history')}}</h1>
                        {!!__('about.text_history')!!}
                    </div>
                    <div class="col-lg-3">

                        <a href="{{route('front.business')}}#certificaciones" class="icon">
                            <div class="img" style="background-image:url('{{asset('assets/img/about/certificaciones-icon.jpg')}}')"></div>
                            <h4>{{__('about.certificate')}}</h4>
                        </a>

                        <a href="{{route('front.installation')}}" class="icon">
                            <div class="img" style="background-image:url('{{asset('assets/img/about/instalaciones-icon.jpg')}}')"></div>
                            <h4>{{__('about.installation')}}</h4>
                        </a>

                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

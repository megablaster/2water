@extends('layouts.front')
@section('title', $category->name)

@push('head')
    <link rel="stylesheet" href="{{ asset('plugins/owl.carousel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/owl.carousel/dist/assets/owl.theme.default.min.css') }}">
@endpush

@section('content')
    <section id="header-product" style="background-image:url('{{asset('assets/img/home-info.jpeg')}}')">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <h2>
                        @switch($category->id)
                            @case('1')
                                {{__('category.category_1')}}
                                @break
                            @case('2')
                                {{__('category.category_2')}}
                                @break
                            @case('3')
                                {{__('category.category_3')}}
                                @break
                            @case('4')
                                {{__('category.category_4')}}
                                @break
                            @case('5')
                                {{__('category.category_5')}}
                                @break
                            @case('6')
                                {{__('category.category_6')}}
                                @break
                            @case('7')
                                {{__('category.category_7')}}
                                @break
                            @case('8')
                                {{__('category.category_8')}}
                                @break
                            @default
                                {{__('category.category_9')}}
                        @endswitch
                    </h2>
                </div>
            </div>
        </div>
    </section>
    <section id="search-product">
        <div class="container">
            <div class="row">

                <div class="col-lg-3">
                    <!--Filters-->
                    <form action="{{route('front.search')}}" class="widget widget-search">
                        <div class="form-group">
                            <label>{{__('main.keywords')}}:</label>
                            <input type="text" name="search" class="form-control" value="{{@$search}}">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">{{__('main.search')}}</button>
                    </form>

                    <div class="widget widget-category">
                        <h3>{{__('main.category')}}</h3>
                        <ul>
                            @foreach($categories_global as $category)
                                <li>
                                    <a href="{{route('front.category',$category->url)}}">
                                        @switch($category->id)
                                            @case('1')
                                                {{__('category.category_1')}}
                                                @break
                                            @case('2')
                                                {{__('category.category_2')}}
                                                @break
                                            @case('3')
                                                {{__('category.category_3')}}
                                                @break
                                            @case('4')
                                                {{__('category.category_4')}}
                                                @break
                                            @case('5')
                                                {{__('category.category_5')}}
                                                @break
                                            @case('6')
                                                {{__('category.category_6')}}
                                                @break
                                            @case('7')
                                                {{__('category.category_7')}}
                                                @break
                                            @case('8')
                                                {{__('category.category_8')}}
                                                @break
                                            @default
                                                {{__('category.category_9')}}
                                        @endswitch
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="col-lg-9">
                    <!--Products-->
                    <div class="row">
                        @if($products->count() > 0)
                            @foreach($products as $product)
                                <div class="col-lg-4">
                                    <a href="{{route('front.single',[Str::slug($product->category->name),$product->url])}}">
                                        <div class="item-product">
                                            <div class="img" style="background-image: url('{{route('get.image',$product->img)}}')">
                                                @if(!empty($product->sku))
                                                    <div class="sku">{{$product->sku}}</div>
                                                @endif
                                            </div>
                                            <div class="info">
                                                <h3>{{$product->name}}</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach

                            <div class="col-xl-12 text-center">
                                {{$products->links() }}
                            </div>

                        @else
                            <div class="col-xl-12">
                                <h2 class="text-center">
                                    No hay productos en la categoría.
                                </h2>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

<!-- push external js -->
@push('script')
    <script src="{{ asset('plugins/owl.carousel/dist/owl.carousel.min.js') }}"></script>
@endpush

@extends('layouts.front')
@section('title', 'Bolsa de trabajo')

@push('head')
@endpush

@section('content')
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <h1>{{__('main.title_job')}}</h1>
                    @if (Session::has('success'))
                        <div class="alert alert-success">
                            {!! Session::get('success') !!}
                        </div>
                    @endif
                    <form id="form" method="post" action="{{route('send.job')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>{{__('main.form.name')}}</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>{{__('main.form.email')}}</label>
                            <input type="email" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>{{__('main.form.phone')}}</label>
                            <input type="tel" name="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>{{__('main.form.cv')}}</label>
                            <input type="file" name="archive" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-submit">{{__('main.submit')}}</button>
                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-6">
                    <div class="icon">
                        <div class="text">
                            <i class="fas fa-globe-africa fa-4x"></i>
                            <h4>{{__('main.direction')}}</h4>
                            <p>Bellavista 42, Col. San Juan Xalpa, Iztapalapa, CP. 09850, CDMX, México.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <div class="icon">
                        <div class="text">
                            <i class="fas fa-headphones-alt fa-4x"></i>
                            <h4>{{__('main.sales')}}</h4>
                            <ul>
                                <li>
                                    <a href="tel:5541961919">(55) 4196 1919</a>
                                </li>
                                <li>
                                    <a href="tel:018000873287">01 800 087 32 87</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6">
                    <div class="icon">
                        <div class="text">
                            <i class="fas fa-envelope-open-text fa-4x"></i>
                            <h4>{{__('main.form.email')}}</h4>
                            <a href="mailto:rh@2water.com.mx">rh@2water.com.mx</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

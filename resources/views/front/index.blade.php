@extends('layouts.front')
@section('title', 'Inicio')

@push('head')
    <link rel="stylesheet" href="{{ asset('plugins/owl.carousel/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/owl.carousel/dist/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
@endpush

@section('content')
    <section id="home">
        <section id="slide">
            <div class="owl-carousel owl-theme owl-slide-home">

                @foreach($sliders as $slider)
                    <div class="item">
                        @if ($slider->select == 'archive')
                            @if ($slider->archive)
                                <a href="{{route('get.image',$slider->archive)}}" target="_blank">
                                    <img src="{{$slider->imagefull}}" class="img-fluid">
                                </a>    
                            @else
                                <img src="{{$slider->imagefull}}" class="img-fluid">
                            @endif
                        @elseif($slider->select == 'url')
                            <a href="{{$slider->url}}" target="_blank">
                                <img src="{{$slider->imagefull}}" class="img-fluid">
                            </a>
                        @else
                            <img src="{{$slider->imagefull}}" class="img-fluid">
                        @endif
                    </div>
                @endforeach
                
            </div>
        </section>
        <section id="info" style="background-image: url('{{asset('/assets/img/home-info.jpeg')}}')">
            <div class="container">
                <div class="row">
                    <div class="col-xl-10 offset-xl-1 text-center">

                        <h2>{{__('home.title')}}</h2>

                        <div class="row">
                            <div class="col-md-4">

                                <a href="{{route('front.business')}}#promocionales" class="cube">
                                    <i class="fas fa-shopping-bag fa-5x"></i>
                                    <h4>{{__('home.promotional')}}</h4>
                                </a>

                            </div>
                            <div class="col-md-4">

                                <a href="{{route('catalog.digital')}}" class="cube">
                                    <i class="fas fa-book-open fa-5x"></i>
                                    <h4>{{__('home.catalog')}}</h4>
                                </a>

                            </div>
                            <div class="col-md-4">

                                <a href="{{route('front.services')}}#desarrollo-de-productos" class="cube">
                                    <i class="fas fa-cogs fa-5x"></i>
                                    <h4>{{__('home.specials')}}</h4>
                                </a>

                            </div>
                        </div>

                        @if (session()->get('locale') == 'en')
                            <h5>Custom made plastic products</h5>
                            <h3>Maquila, <span>Development</span>, Molds</h3>
                        @endif

                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 p-0">
                        <div class="owl-carousel owl-theme owl-promo">
                            <div class="item">
                                <a href="{{asset('assets/img/01-min.jpg')}}" data-fancybox="gallery">
                                    <div class="img" style="background-image: url('{{asset('assets/img/01-min.jpg')}}')"></div>
                                </a>
                            </div>
                            <div class="item">
                                <a href="{{asset('assets/img/02-min.jpg')}}" data-fancybox="gallery">
                                    <div class="img" style="background-image: url('{{asset('assets/img/02-min.jpg')}}')"></div>
                                </a>
                            </div>
                            <div class="item">
                                <a href="{{asset('assets/img/03-min.jpg')}}" data-fancybox="gallery">
                                    <div class="img" style="background-image: url('{{asset('assets/img/03-min.jpg')}}')"></div>
                                </a>
                            </div>
                            <div class="item">
                                <a href="{{asset('assets/img/06-min.jpg')}}" data-fancybox="gallery">
                                    <div class="img" style="background-image: url('{{asset('assets/img/06-min.jpg')}}')"></div>
                                </a>
                            </div>
                            <div class="item">
                                <a href="{{asset('assets/img/07-min.jpg')}}" data-fancybox="gallery">
                                    <div class="img" style="background-image: url('{{asset('assets/img/07-min.jpg')}}')"></div>
                                </a>
                            </div>
                            <div class="item">
                                <a href="{{asset('assets/img/08-min.jpg')}}" data-fancybox="gallery">
                                    <div class="img" style="background-image: url('{{asset('assets/img/08-min.jpg')}}')"></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="info2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 bg-black">
                        <div class="text">
                            <h3>{!!__('home.video')!!}</h3>
                            <a href="https://www.youtube.com/watch?v=2XGsP6ETTZ0?rel=0&controls=1&modestbranding=1&autoplay=1" data-fancybox class="btn btn-green">{{__('home.video_button')}}</a>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="img" style="background-image: url('{{asset('assets/img/05-min.jpg')}}')"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2 p-0">
                        <div class="img" style="background-image: url('{{asset('assets/img/07-min.jpg')}}')"></div>
                    </div>
                    <div class="col-lg-6 bg-black order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1">
                        <div class="text">
                            <h3>{!!__('home.about_title')!!}</h3>
                            <p>{{__('home.about_text')}}</p>
                            <a href="{{route('front.about')}}" class="btn btn-green">{{__('home.read_more')}}</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 bg-black">
                        <div class="text">
                            <h3>{!!__('home.installation_title')!!}</h3>
                            <p>{{__('home.installation_text')}}</p>
                            <a href="{{route('front.installation')}}" class="btn btn-green">{{__('home.read_more')}}</a>
                        </div>
                    </div>
                    <div class="col-lg-6 p-0">
                        <div class="img" style="background-image: url('{{asset('assets/img/04-min.jpg')}}')"></div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

<!-- push external js -->
@push('script')
    <script src="{{ asset('plugins/owl.carousel/dist/owl.carousel.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script>
        $('.owl-promo').owlCarousel({
            loop:true,
            margin:35,
            dots:false,
            autoplay:true,
            nav:true,
            center:true,
            navText: ['<span class="fas fa-chevron-left fa-2x"></span>','<span class="fas fa-chevron-right fa-2x"></span>'],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                },
                1366:{
                    items:3
                },
                1367:{
                    items:4
                }
            }
         });

        $('[data-fancybox="gallery"]').fancybox({
        });

        $('.owl-slide-home').owlCarousel({
            autoplay:true,
            loop:true,
            margin:0,
            dots:true,
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    </script>
@endpush

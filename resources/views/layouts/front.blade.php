<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
<head>
    <title>@yield('title','') | 2Water Plásticos</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('include.head')
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('favicon.png') }}" type="image/x-icon" />
    <!-- themekit admin template asstes -->
    <link rel="stylesheet" href="{{ asset('all.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @yield('head')
</head>
<body>
    @include('include.navbar')
    @yield('content')
    @stack('script')
    @include('include.footer')
</body>
</html>


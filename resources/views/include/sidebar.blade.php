<div class="app-sidebar colored">
    <div class="sidebar-header">
        <a class="header-brand" href="{{route('dashboard')}}">
            <div class="logo-img text-center">
               {{-- <img height="30" src="{{ asset('img/logo_white.png')}}" class="header-brand-img" title="RADMIN">  --}}
               <h2 style="margin-top:10px; margin-left: 10px;">2Water</h2>
            </div>
        </a>
        <div class="sidebar-action"><i class="ik ik-arrow-left-circle"></i></div>
        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
    </div>
    @php
        $segment1 = request()->segment(1);
        $segment2 = request()->segment(2);
    @endphp

    <div class="sidebar-content">
        <div class="nav-container">
            <nav id="main-menu-navigation" class="navigation-main">
                <div class="nav-item {{ ($segment1 == 'dashboard') ? 'active' : '' }}">
                    <a href="{{route('dashboard')}}"><i class="ik ik-bar-chart-2"></i><span>{{ __('Estadísticas')}}</span></a>
                </div>
                @can('manage_user')
                    <div class="nav-item {{ ($segment1 == 'users' || $segment1 == 'roles' ||$segment1 == 'permission' || $segment1 == 'user') ? 'active open' : '' }} has-sub">
                        <a href="#"><i class="ik ik-user"></i><span>{{ __('Usuarios')}}</span></a>
                        <div class="submenu-content">
                            @can('manage_user')
                                <a href="{{url('users')}}" class="menu-item {{ ($segment1 == 'users') ? 'active' : '' }}">{{ __('Usuarios')}}</a>
                                <a href="{{url('user/create')}}" class="menu-item {{ ($segment1 == 'user' && $segment2 == 'create') ? 'active' : '' }}">{{ __('Agregar usuario')}}</a>
                            @endcan
                        </div>
                    </div>
                @endcan
                @can('manage_slider')
                    <div class="nav-item {{ ($segment1 == 'slides') ? 'active open' : '' }} has-sub">
                        <a href="#"><i class="ik ik-image"></i><span>{{ __('Rotador')}}</span></a>
                        <div class="submenu-content">
                            @can('manage_product')
                                <a href="{{url('sliders')}}" class="menu-item {{ ($segment1 == 'slides') ? 'active' : '' }}">{{ __('Rotadores')}}</a>
                                <a href="{{url('slider/create')}}" class="menu-item {{ ($segment1 == 'product' && $segment2 == 'create') ? 'active' : '' }}">{{ __('Agregar rotador')}}</a>
                            @endcan
                        </div>
                    </div>
                @endcan
                @can('manage_product')
                    <div class="nav-item {{ ($segment1 == 'colors') ? 'active open' : '' }} has-sub">
                        <a href="#"><i class="ik ik-list"></i><span>{{ __('Colores')}}</span></a>
                        <div class="submenu-content">
                            @can('manage_product')
                                <a href="{{url('colors')}}" class="menu-item {{ ($segment1 == 'colors') ? 'active' : '' }}">{{ __('Colores')}}</a>
                                <a href="{{url('color/created')}}" class="menu-item {{ ($segment1 == 'color' && $segment2 == 'created') ? 'active' : '' }}">{{ __('Agregar color')}}</a>
                            @endcan
                        </div>
                    </div>
                @endcan
                @can('manage_product')
                    <div class="nav-item {{ ($segment1 == 'products') ? 'active open' : '' }} has-sub">
                        <a href="#"><i class="ik ik-code"></i><span>{{ __('Productos')}}</span></a>
                        <div class="submenu-content">
                            @can('manage_product')
                                <a href="{{url('products')}}" class="menu-item {{ ($segment1 == 'products') ? 'active' : '' }}">{{ __('Productos')}}</a>
                                <a href="{{url('product/create')}}" class="menu-item {{ ($segment1 == 'product' && $segment2 == 'create') ? 'active' : '' }}">{{ __('Agregar producto')}}</a>
                            @endcan
                        </div>
                    </div>
                @endcan
                <div class="nav-item {{ ($segment1 == 'leads') ? 'active' : '' }}">
                    <a href="{{url('leads')}}"><i class="ik ik-mail"></i><span>{{ __('Bolsa de trabajo')}}</span></a>
                </div>
                <div class="nav-item {{ ($segment1 == 'orders') ? 'active' : '' }}">
                    <a href="{{url('orders')}}"><i class="ik ik-database"></i><span>{{ __('Ordenes')}}</span></a>
                </div>
                <div class="nav-item {{ ($segment1 == 'systems') ? 'active' : '' }}">
                    <a href="{{route('system.edit',1)}}"><i class="ik ik-link"></i><span>{{ __('Catálogo PDF')}}</span></a>
                </div>
            </nav>
        </div>
    </div>

</div>

<h2>Política de reclamo y devolución de producto</h2>
<ul>
<li>I. Solo se aceptarán devoluciones y reclamos gestionados a través de nuestra web: www.2water.com.mx</li>
<li>II. El plazo para la presentación de dichos reclamos será dentro de los 15 (quince) días
fecha factura, vencido ese plazo 2W Plásticos no asume ninguna responsabilidad al
respecto, sin excepción alguna.</li>
<li>III. 2W Plásticos podría analizar y aceptar reclamaciones por las siguientes razones:
    <ul>
        <li>Material dañado. </li>
        <li>Material faltante. </li>
        <li>Material equivocado.</li>
        <li>Material fuera de especificación. </li>
        <li>Material extra no solicitado. </li>
        <li>Diferencia en precios.</li>
        <li>Error o cancelación por parte del cliente (si se autoriza, generará penalización).</li>
        <li>Reclamación por garantía de uso (únicamente para titular de factura original).</li>
        <li>Re facturación por entrega fuera de tiempo y/o error de datos, imputable a 2W</li>
        Plásticos.
    </ul>
</li>
<li>IV. 2W Plásticos no aceptará devolución o reclamaciones en los siguientes casos:
    <ul>
        <li>El cliente se equivocó de código al ordenar.</li>
        <li>El cliente se equivocó al confirmar su pedido.</li>
        <li>El cliente duplicó su confirmación.</li>
        <li>No le sirvió a cliente el material para su objetivo.</li>
        <li>Por cancelación de facturas (re facturación).</li>
        <li>Por errores en datos fiscales (dirección, RFC, razón social o nombre, etc.)</li>
    </ul>
</li>
<li>V. No se aceptarán devoluciones cuando la mercancía haya sido solicitada especialmente por el cliente (artículos sobre pedido), salvo que se encuentre dañada o incompleta desde origen.</li>
<li> VI. Solo se aceptarán reclamos por faltantes cuando los mismos sean responsabilidad de 2W Plásticos y entren dentro del marco de garantías vigentes de la empresa.</li>
<li>VII. Razones por las cuales se podrá rechazar una reclamación:
    <ul>
        <li>a. Reclamaciones fuera de tiempo (15 días naturales).</li>
        <li>b. Sin fundamento para la reclamación.</li>
        <li>c. Reclamaciones de clientes de terceros o corresponde el reclamo a un cliente distinto
        al que se le facturó el material.</li>
    </ul>
</li>
<li>VIII. Ingresando en nuestra página web (2water.com.mx), en la sección contacto, podrá ingresar su formulario online. Completando cada uno de los campos, sin faltante de información.</li>
<li>IX. 2W Plásticos dispone de 72 horas para analizar y responder el reclamo.</li>
<li>X. Una vez recibido el material, se analizará calidad y cantidad tanto del producto como del empaque. Se determinará si el material cumple con las condiciones correspondientes. De no ser así se les notificará para el retiro del mismo.
    <ul>
        <li>Código: DCO-PO-01 Rev.0</li>
    </ul>
</li>
<li>XI. El embalaje del material enviado por parte del cliente debe encontrarse en perfectas condiciones.</li>
<li>XII. 2W Plásticos no es responsable por mano de obra o algún otro gasto que haga el Comprador por motivo de desmontaje, reparación o reemplazo de los productos reclamados como defectuosos, ni tampoco por cualquier otro gasto hecho por el cliente al tratar de remediar cualquier defecto.</li>
<li>XIII. Si el caso requiere recolección de material. El cliente deberá obtener autorización por escrito de parte del área de reclamaciones.</li>
<li>XIV. Tratándose de errores de 2W Plásticos, las devoluciones únicamente se podrán hacer con nuestras paqueterías a elección y convenio. Cualquier otro medio utilizado generará un cargo por el valor del flete al cliente.</li>
<li>XV. Tratándose de errores del Cliente, el cliente deberá enviar la mercancía por la paquetería de su elección sin ningún cargo para 2W Plásticos.</li>
<li>XVI. 2W Plásticos esta sujetó a las reglas y políticas de los transportistas o paqueterías.</li>
<li>XVII. Por otros motivos por intención de devolución deben tratarse con su Asesor de ventas.</li>
</ul>
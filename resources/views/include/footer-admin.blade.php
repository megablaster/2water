<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 text-center">
                <p style="margin: 0;">{{ __('Copyright © '.date("Y").' - 2Water Plásticos')}}</p>
            </div>
        </div>
    </div>
</footer>

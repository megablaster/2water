<div id="navbar">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <a href="{{route('front.index')}}">
                    <img src="{{asset('assets/img/logo.png')}}" class="img-fluid logo">
                </a>
            </div>
            <div class="col-lg-8 p-relative">
                <ul class="nav-bar">
                    <li style="display:none;"><a href="{{route('front.index')}}">{{__('main.menu.home')}}</a></li>
                    <li><a href="{{route('front.products')}}">{{__('main.menu.products')}} <i class="fas fa-chevron-down"></i></a>
                        <ul class="sub-menu">
                             @foreach($categories_global as $category)
                                <li>
                                    <a href="{{route('front.category',$category->url)}}">
                                        @switch($category->id)
                                            @case('1')
                                                <i class="fas fa-arrow-right"></i> {{__('category.category_1')}}
                                                @break
                                            @case('2')
                                                <i class="fas fa-arrow-right"></i> {{__('category.category_2')}}
                                                @break
                                            @case('3')
                                                <i class="fas fa-arrow-right"></i> {{__('category.category_3')}}
                                                @break
                                            @case('4')
                                                <i class="fas fa-arrow-right"></i> {{__('category.category_4')}}
                                                @break
                                            @case('5')
                                                <i class="fas fa-arrow-right"></i> {{__('category.category_5')}}
                                                @break
                                            @case('6')
                                                <i class="fas fa-arrow-right"></i> {{__('category.category_6')}}
                                                @break
                                            @case('7')
                                                <i class="fas fa-arrow-right"></i> {{__('category.category_7')}}
                                                @break
                                            @case('8')
                                                <i class="fas fa-arrow-right"></i> {{__('category.category_8')}}
                                                @break
                                            @default
                                                <i class="fas fa-arrow-right"></i> {{__('category.category_9')}}
                                        @endswitch
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <li><a href="{{route('catalog.digital')}}">{{__('main.menu.catalog')}}</a>
                    <li><a href="{{route('front.business')}}">{{__('main.menu.business_line')}} <i class="fas fa-chevron-down"></i></a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('front.business')}}#header">
                                    <i class="fas fa-arrow-right"></i> {{__('business.promotion.title')}}
                                </a>
                            </li>
                            <li>
                                <a href="{{route('front.business')}}#info1"><i class="fas fa-arrow-right"></i> {{__('business.retail.title')}}
                                </a>
                            </li>
                            <li style="display: none;">
                                <a href="{{route('front.business')}}#maquilas"><i class="fas fa-arrow-right"></i> Maquilas</a>
                            </li>
                            <li>
                                <a href="{{route('front.business')}}#exportacion"><i class="fas fa-arrow-right"></i> {{__('business.export.title')}}</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="{{route('front.services')}}">{{__('main.menu.services')}} <i class="fas fa-chevron-down"></i></a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('front.services')}}#maquilas">
                                    <i class="fas fa-arrow-right"></i> {{__('services.title_maquila')}}
                                </a>
                            </li>
                            <li>
                                <a href="{{route('front.services')}}#desarrollo-de-productos">
                                    <i class="fas fa-arrow-right"></i> {{__('services.title_product')}}
                                </a>
                            </li>
                            <li>
                                <a href="{{route('front.services')}}#fabricacion-de-moldes">
                                    <i class="fas fa-arrow-right"></i> {{__('services.title_moldes')}}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="{{route('front.about')}}">{{__('main.menu.about')}}</a></li>
                    <li><a href="{{route('front.contact')}}">{{__('main.menu.contact')}} <i class="fas fa-chevron-down"></i></a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{route('front.contact')}}">
                                    <i class="fas fa-arrow-right"></i> {{__('main.menu.contact')}}
                                </a>
                            </li>
                            <li>
                                <a href="{{route('front.job')}}">
                                    <i class="fas fa-arrow-right"></i> {{__('main.menu.job')}}
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-lg-2 p-relative">
                <ul class="lang-inline">
                    <li><a href="#" data-lang="es" class="link-lang">Español</a></li>
                    <li>|</li>
                    <li><a href="#" data-lang="en" class="link-lang">English</a></li>
                    <form action="{{route('lang.changeinline')}}" method="post" id="lang-form">
                        @csrf
                        <input type="hidden" value="" id="lang_refill" name="lang">
                    </form>
                </ul>
                <form action="{{route('front.search')}}" class="search-top">
                    <input type="text" name="search" class="form-control" placeholder="{{ __('main.search_product') }}">
                    <button type="submit" class="btn btn-search"><i class="fas fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="menu-mobile">
    <div class="container">
        <div class="row">
            <div class="col-3">
                <a href="{{route('front.index')}}">
                    <img src="{{asset('assets/img/logo.png')}}" class="img-fluid logo">
                </a>
            </div>
            <div class="col-2 offset-7">
                <i class="fas fa-bars fa-3x" id="click-mobile"></i>
            </div>
        </div>
    </div>
</div>

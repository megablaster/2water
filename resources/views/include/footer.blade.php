<div id="politica" style="display: none;max-width: 800px;">
    @include('include.political')
</div>

<div id="sub-footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 offset-xl-1 col-md-6">
                <div class="info">
                <img src="{{asset('assets/img/logo.png')}}" class="img-fluid logo">
                <img src="{{asset('assets/img/brands.png')}}" class="img-fluid brands">
                </div>
            </div>
            <div class="col-xl-2 col-md-6">
                <div class="info">
                <h3>{{__('main.main_menu')}}</h3>
                <ul class="points">
                    <li><a href="{{route('front.index')}}">{{__('main.menu.home')}}</a></li>
                    <li><a href="{{route('front.products')}}">{{__('main.menu.products')}}</a></li>
                    <li><a href="{{route('front.business')}}">{{__('main.menu.business_line')}}</a></li>
                    <li><a href="{{route('front.services')}}">{{__('main.menu.services')}}</a></li>
                    <li><a href="{{route('front.about')}}">{{__('main.menu.about')}}</a></li>
                    <li><a href="{{route('front.contact')}}">{{__('main.menu.contact')}}</a></li>
                </ul>
                </div>
            </div>
            <div class="col-xl-2 col-md-6">
                <div class="info">
                <h3>{{__('main.category')}}</h3>
                <ul class="points">
                    @foreach($categories_global as $category)
                        <li>
                            <a href="{{route('front.category',$category->url)}}">
                                @switch($category->id)
                                    @case('1')
                                        {{__('category.category_1')}}
                                        @break
                                    @case('2')
                                        {{__('category.category_2')}}
                                        @break
                                    @case('3')
                                        {{__('category.category_3')}}
                                        @break
                                    @case('4')
                                        {{__('category.category_4')}}
                                        @break
                                    @case('5')
                                        {{__('category.category_5')}}
                                        @break
                                    @case('6')
                                        {{__('category.category_6')}}
                                        @break
                                    @case('7')
                                        {{__('category.category_7')}}
                                        @break
                                    @case('8')
                                        {{__('category.category_8')}}
                                        @break
                                    @default
                                        {{__('category.category_9')}}
                                @endswitch
                            </a>
                        </li>
                    @endforeach
                </ul>
                </div>
            </div>
            <div class="col-xl-2 col-md-6">
                <div class="info">
                   <img src="{{asset('assets/img/logo_esr.png')}}" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3">
                <p>{{ __('Copyright © '.date("Y").' 2Water Plásticos')}}.</p>
            </div>
            <div class="col-xl-9">
                <ul>
                    <li><a href="#politica" data-fancybox>{{__('main.menu.politica')}}</a></li>
                    <li>|</li>
                    <li><a href="{{asset('pdf/avisodeprivacidad.pdf')}}" target="_blank">{{__('main.footer.aviso')}}</a></li>
                    <li>|</li>
                    <li><a href="{{asset('pdf/formatoderechosarco.pdf')}}" target="_blank">{{__('main.footer.derechos')}}</a></li>
                    <li>|</li>
                    <li><a href="{{asset('pdf/revocacion.pdf')}}" target="_blank">{{__('main.footer.revocacion')}}</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<script src="{{asset('js/front.js')}}"></script>

<ul class="float-social">
    <li>
        <a href="https://api.whatsapp.com/send?phone=525533048958&text=Me%20interesa%20saber%20m%C3%A1s%20acerca%20de%20los%20servicios%20de%202W%20Pl%C3%A1sticos" target="_blank">
            <i class="fab fa-whatsapp fa-2x"></i>
        </a>
    </li>
    <li>
        <a href="https://www.facebook.com/2wplasticos" target="_blank">
            <i class="fab fa-facebook fa-2x"></i>
        </a>
    </li>
    <li>
        <a href="#" target="_blank">
            <i class="fab fa-instagram fa-2x"></i>
        </a>
    </li>
    <li>
        <a href="https://www.linkedin.com/company/2water-plasticos" target="_blank">
            <i class="fab fa-linkedin-in fa-2x"></i>
        </a>
    </li>
</ul>


<?php

return [

    'search' => 'Buscar',
    'search_product' => 'Producto o clave...',
    'results' => 'Resultados de la búsqueda',
    'keywords' => 'Palabra de búsqueda',
    'search' => 'Buscar',
    'category' => 'Categorías',
    'main_menu' => 'Menú',
    'phrase' => 'Consulta mínimos de fabricación con tu asesor.',
    'information' => 'Solicitar información',
    'info_aditional' => 'Información adicional',
    'submit' => 'Enviar',
    'title' => 'Dudas, información o quejas <span style="font-weight:700;">¡Contáctanos!</span>',
    'title_job' => 'Bolsa de Trabajo',
    'direction' => 'Dirección',
    'sales' => 'Teléfono',
    'sell' => 'Ventas',
    'menu' => [
        'home' => 'Inicio',
        'products' => 'Productos',
        'business_line' => 'Líneas de Negocio',
        'services' => 'Servicios',
        'about' => 'Nosotros',
        'contact' => 'Contacto',
        'job' => 'Bolsa de trabajo',
        'politica' => 'Politica de reclamaciones',
        'catalog' => 'Catálogo'
    ],
    'info' => [
        'name' => 'Nombre',
        'capacity' => 'Capacidad',
        'description' => 'Descripción',
        'color' => 'Colores de línea',
        'services' => 'Servicios',
        'microowave' => 'Resistencia a microondas',
        'measure' => 'Medidas de producto',
    ],
    'form' => [
        'name' => 'Nombre completo',
        'company' => 'Empresa',
        'email' => 'Correo electrónico',
        'phone' => 'Teléfono',
        'color' => '-- Selecciona el color --',
        'quantity' => 'Cantidad',
        'comments' => 'Comentarios',
        'cv' => 'Subir CV en PDF'
    ],
    'footer' => [
        'aviso' => 'Aviso de Privacidad',
        'derechos' => 'Formato Derechos Arco',
        'revocacion' =>'Formato de Revocación del Consentimiento',
        'reservados' => 'Todos los derechos reservados'
    ]

];

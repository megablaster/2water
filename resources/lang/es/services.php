<?php

return [
    'title' => 'Servicios',
    'title_maquila' => 'Maquilas',
    'text_maquila' => '
    <p>Dada nuestra amplia experiencia en inyección y soplado, usted puede estar seguro que sus moldes están en las mejores manos, tenemos una amplia línea de producción tanto para inyección como para soplado y soplado por extrusión. Podemos trabajar un amplio rango y tipos de moldes adaptándolos a nuestros procesos.</p>
    <p>Ofrecemos precios altamente competitivos y garantizamos la calidad y duración de sus productos y moldes. Contamos con maquinaria diversa en tipo de proceso y capacidad de inyección o soplado, así como una amplia experiencia con una alta gama de termo plásticos</p>
    <p>Cuando moldes externos nos son asignados para maquila, primero que nada, realizamos un diagnostico general y reporte de posibles daños o fallas. En caso de requerirlo ofrecemos servicios de reparación y realización de trabajos necesarios para que el molde vuelva a estar 100% funcional. Posteriormente mientras el molde está bajo nuestro resguardo todos los mantenimientos, servicios y reparaciones están incluidos en el precio de la maquila, por lo que nuestros clientes no tienen que preocuparse por esto.</p>',
    'title_product' => 'Desarrollo de productos',
    'text_product' => '
    <p>Contamos con un equipo especializado y dedicado al desarrollo de nuevos productos, nuestro personal está altamente capacitado, utilizamos modernos programas de diseño industrial, así como equipos especializados para el escaneo e impresión en formato 3D, esto nos permite lograr con gran precisión y exactitud la presentación de muestras de preproducción. Todo lo anterior consolida nuestra amplia experiencia en el departamento de ingeniería de producto.</p>
    <p>Nuestro enfoque a la mejora continua y actualización de procesos nos permite ofrecer un servicio que inicia en el diseño industrial, pasando por análisis de fluidos, mecánica operativa, hasta el desarrollo y detallado del molde. Contamos con maquinaria líder en su categoría CNC Hass, Tornos convencionales, Fresadoras, electroerosionadoras, cabina de “sandblast”, horno de templado y más equipo especializado para poder materializar cualquier proyecto.</p>',
    'title_moldes' => 'Fabicación de moldes',
    'text_moldes' => '
    <p>En 2W contamos con más de 10 años de experiencia en el diseño, desarrollo y fabricación de moldes de inyección y soplado de plástico. Contamos con maquinaría y equipo con tecnología de punta.</p>
    <p>Nuestro enfoque a la mejora continua y actualización de procesos nos permite ofrecer un servicio que inicia en el diseño industrial, pasando por análisis de fluidos, mecánica operativa, hasta el desarrollo y detallado del molde. Contamos con maquinaria líder en su categoría CNC Hass, Tornos convencionales, Fresadoras, electroerosionadoras, cabina de “sandblast”, horno de templado y más equipo especializado para poder materializar cualquier proyecto.</p>',

];

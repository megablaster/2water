<?php

return [
    'title' => 'Instalaciones',
    'plant_title' => 'Planta Productiva',
    'plant_text' => '
                <h4>Inyección</h4>
                <p>Máquinas de Inyección que van de las 80 a las 300 toneladas</p>
                <h4>Inyección-Soplo</h4>
                <p>Máquinas de última generación de inyección soplo de una sola etapa</p>
                <h4>Extrusion-Soplo</h4>
                <p>Máquinas con capacidad de 500 mililitros hasta 2 litros</p>
    ',
    'taller_title' => 'Taller de Moldes',
    'taller_text' => 'Taller especializado en reparación y desarrollo de moldes, equipado con maquinaria de primer nivel que consta desde Centros de maquinado hasta hornos de templado, pasando por equipo convencional, impresoras 3D y herramientas de alta calidad.',
    'laboratory_title' => 'Laboratorio de calidad',
    'laboratory_text' => '
            <p>Provisto con equipos de alta precisión, nuestro laboratorio cumple con los estándares internacionales para la evaluación y aseguramiento de calidad en todos los procesos productivos que manejamos, logrando así niveles minimos de rechazo y asegurando una trazabilidad de producto desde nuestros proveedores de materia prima, hasta el almacén de nuestros clientes.</p>
            <p>Todas las instalaciones de 2W Plásticos cuentan con certificaciones de calidad, procesos y responsabilidad social, con lo cual aseguramos tanto la calidad y seguridad de todos nuestros productos, así como la de todos los que formamos parte del equipo de trabajo.</p>
    '
];

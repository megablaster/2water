<?php
    
    return [
        'category_1' => 'Cocina y hogar',
        'category_2' => 'Vasos y termos',
        'category_3' => 'Bebés y niños',
        'category_4' => 'Recreación',
        'category_5' => 'Cilindros',
        'category_6' => 'Salud y seguridad',
        'category_7' => 'Restaurante y hotel',
        'category_8' => 'Botellas',
        'category_9' => 'Tapas',
    ];
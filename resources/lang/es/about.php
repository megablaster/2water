<?php
    
    return [
        'title' => 'Nosotros',
        'title_history' => 'Nuestra historia',
        'text_history' => '
        <p>La empresa nace en 2007 para satisfacer una necesidad creciente en el mercado de artículos promocionales en México. Una empresa con más de 10 años de experiencia en la transformación de plástico, desarrollando productos innovadores de alta calidad líderes en el mercado de promocionales del país. Trabajamos con los más distinguidos importadores y distribuidores de promocionales, así como empresas de consumo masivo y empresas líderes en venta por catálogo de fama internacional.</p>
        <p>2W Plásticos tiene un enfoque dedicado a satisfacer las necesidades de cada cliente, sin importar el tipo de producto ni mercado al que va dirigido siempre encontramos la manera de desarrollar el producto adecuado al precio correcto para lograr los resultados que buscan nuestros clientes.</p>
        <p>Hoy en día contamos con instalaciones de primer nivel certificadas por organismos internacionales, cumpliendo con normas y estándares enfocados a la calidad, los procesos y, por supuesto, la responsabilidad social. Así mismo, contamos con una alta autonomía en nuestros departamentos internos para poder dar servicio, mantenimiento y reparación a nuestra maquinaria de forma inmediata, para lograr así cumplir con los compromisos comerciales que adquirimos.</p>
        <p>En 2016 comenzamos a exportar producto a Estados Unidos, en 2017 a Centroamérica y en 2018 nuestros productos llegan ya a Sudamérica. También contamos con un taller de fabricación de moldes local, el cual nos permite ofrecer soluciones y desarrollos a la medida con una excelente velocidad de reacción.</p>',
        'certificate' => 'Certificaciones',
        'installation' => 'Instalaciones'
    ];
<?php

return [

    'title' => 'La Fábrica de Plásticos No. 1 de Artículos Promocionales',
    'catalog' => 'Ver catálogo',
    'promotional' => 'Promocionales',
    'specials' => 'Desarrollos especiales',
    'video' => 'Mostrar <span>video</span>',
    'video_button' => 'Mostrar video',
    'about_title' => 'Acerca de <span>Nosotros</span>',
    'about_text' => 'La empresa nace en 2007 para satisfacer una necesidad creciente en el mercado de artículos promocionales en México. Una empresa con más de 10 años de experiencia en la transformación de plástico, desarrollando productos innovadores de alta calidad líderes en el mercado de promocionales del país.',
    'read_more' => 'Leer más',
    'installation_title' => 'Nuestras <span>Instalaciones</span>',
    'installation_text' => 'Ubicados al sur de la ciudad de México, nuestras instalaciones constan de más de 6,000m2 de área productiva, la cual trabaja 24 horas los 7 días de la semana.',
    ''

];

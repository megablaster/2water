<?php

return [

    'title' => 'Líneas de negocio',
    'promotion' => [
        'title' => 'Artículos promocionales',
        'text1' => 'Líder nacional en fabricación y diseño de artículos promocionales.',
        'text2' => 'Con más de 10 años de experiencia desarrollando promocionales en 2W Plásticos creemos fuertemente en la innovación, en la industria mexicana, en la calidad y el compromiso con nuestros clientes.',
        'text3' => 'Orgullosamente hoy somos el fabricante #1 de artículos promocionales de plástico en México, contamos con una basta línea de productos y trabajamos con los principales Importadores del País, logrando así disminuir considerablemente las importaciones de China, ofreciendo productos de alta calidad, diseño y precios competitivos.'
    ],
    'retail' => [
        'title' => 'Retail y hogar',
        'text1' => 'Nuestros artículos de Hogar y consumo hoy son reconocidos a nivel nacional, contamos con una gran oferta de productos para autoservicios, venta por catálogo, tiendas departamentales y tiendas de conveniencia, logrando así penetrar en millones de hogares mexicanos. La línea de productos Retail que manejamos está pensada para hacer la vida más practica y cómoda para todos los usuarios.',
        'text2' => 'La calidad y diseño son dos de nuestros principales motores para el desarrollo de esta línea de productos, buscando simplificar y facilitar la vida diaria dentro y fuera del Hogar. Contamos también con una amplia gama de colores y materiales, así como servicios de decorado para hacerlo más atractivo y divertido para los usuarios.',
    ],
    'envase' => [
        'title' => 'Envases para Empaque',
        'text1' => 'Contamos una amplia gama de productos, modelos y materiales destinados para gran variedad de industrias con necesidad de empaques plásticos como la Alimenticia, Cosmética, Farmacéutica, entre muchas otras.',
        'text2' => 'Trabajamos con una amplia cantidad de materiales plásticos como PET, HDPE, LDPE, PP, PS y otros más especializados. Así mismo podemos manejar gran variedad de colores y acabados para asegurar un empaque moderno, útil y de calidad.',
        'text3' => 'Podemos ofrecer productos de nuestro catalogo listos para fabricarse o diseñar moldes a la medida de acuerdo a las necesidades especificas del cliente en cuanto a forma, capacidad, tamaño y tipo de rosca, así como cualquier requerimiento especifico. Del mismo modo podemos asignar líneas de producción especificas por proyecto y ajustarlas a las necesidades y demandas de cada cliente, de esta manera logramos tiempos de entrega de acuerdo a sus necesidades y asignamos la capacidad productiva requerida para el proyecto.',
    ],
    'export' => [
        'title' => 'Exportación',
        'text1' => 'En 2W Plásticos hemos trabajado fuertemente desde nuestros inicios para lograr cumplir con estándares internacionales en todos los productos que manejamos. Gracias a nuestras certificaciones, profesionalismo y capacidad de producción hoy exportamos a más de 15 países del continente.',
        'text2' => 'Exportamos más de 100 diferentes productos desde Argentina hasta Estados unidos. Pasando por:',
    ],
    'certificate' => [
        'title' => 'Certificaciones',
        'text1' => 'Toda nuestra materia prima es 100% virgen con certificaciones de FDA (U.S. Food and Drug Administration), son libres de BPA (Bisfenol A) y cumplen con la Prop 65 de California; Todo los productos que fabricamos son 100% reciclables.',
        'text2' => 'Contamos con certificaciones de organismos internacionales como Bureau Veritas e Intertek para:',
        'text3' => 'Responsabilidad Social',
        'text4' => 'Calidad',
        'text5' => 'Procesos de Manufactura',
        'text6' => 'SQP (Buenas Prácticas de Manufactura (GMP)',
        'text7' => 'Análisis de Modos y Efectos de Fallas (FME)',
        'text8' => 'Orgulloso miembro de',
    ],
    'maquila' => [
        'title' => 'Maquilas',
        'text1' => 'Dada nuestra amplia experiencia en inyección y soplado, usted puede estar seguro de que sus moldes están en las mejores manos, tenemos una amplia línea de producción tanto para inyección como para inyección-soplo y soplado por extrusión. Podemos trabajar un amplio rango y tipos de moldes adaptándolos a nuestros procesos.',
        'text2' => 'Ofrecemos precios altamente competitivos y garantizamos la calidad y duración de sus productos y moldes.',
        'text3' => 'Contamos con maquinaria diversa en tipo de proceso y capacidad de inyección o soplado, así como una amplia experiencia con una alta gama de termo plásticos.',
        'text4' => 'Trabajamos a nivel nacional e internacional con diversas industrias como maquilador de piezas o componentes, para diversas industrias como:',
        'text5' => 'Mueblera',
        'text6' => 'Electrodomésticos',
        'text7' => 'Automotriz',
        'text8' => 'Ferretera',
        'text9' => 'Plomera',
        'text10' => 'Electrónica',
        'text11' => 'Eléctrica',
        'text12' => 'Deportiva',
        'text13' => 'Seguridad',
        'text14' => 'Entre otras...',
        'text15' => 'Inyección',
        'text16' => 'Máquinas de Inyección que van de las 80 a las 300 toneladas',
        'text17' => 'Inyección-Soplo',
        'text18' => 'Máquinas de última generación de inyección soplo de una sola etapa',
        'text19' => 'Extrusion-Soplo',
        'text20' => 'Máquinas con capacidad desde 500 mililitros hasta 2 litros',
    ],
];

<?php

return [
    'title' => 'Maquila and Services',
    'title_maquila' => 'Maquila',
    'text_maquila' => '
    <p>Given our vast experience in injection molding and blow molding, you can be sure that your molds are in the best hands, we have a broad production line for both injection blow molding and extrusion blow molding. We can work with a wide range and types of molds adapting them to our processes.</p>
    <p>We offer highly competitive prices and guarantee the quality and durability of your products and molds.</p>
    <p>We have diverse machinery in process type and injection molding or blow molding capacity, as well as vast experience with a wide range of thermoplastics.</p>
    <p>When we are assigned external molds for maquila, first of all, we carry out a general diagnosis and report possible damages or failures. If required, we offer repair services and all the work necessary for the mold to be 100% functional again. Subsequently, while the mold is under our shelter, all maintenance, services and repairs are included in the price of the maquila, so our customers don’t have to worry about this.</p>',
    'title_product' => 'Product Development',
    'text_product' => '
    <p>We have a specialized team dedicated to the development of new products, our personnel is highly trained, we use modern industrial design programs, as well as specialized equipment for 3D scanning and printing, this allows us to achieve pre-production samples with great precision and accuracy. All of the above reinforces our extensive experience in the product engineering department.</p>
    <p>After the product development and design process, we have an agile interaction communicating the designs to our Industrial Development Center, which has a next generation mold manufacturing workshop and finally materializes in our productive area, where we have thermoplastics processing machinery, that’s where our engineers will materialize what was once just an idea on paper.</p>',
    'title_moldes' => 'Mold Manufacturing',
    'text_moldes' => '
    <p>In 2W we have more than 10 years of experience in the design, development and manufacturing of plastic injection molding and blow molding. We have machinery and equipment with cutting-edge technology.</p>
    <p>Our approach to ongoing improvement and process update allows us to offer a service that begins with industrial design, goes through fluid analysis, operational mechanics, down to the development and detailing of the mold.</p>
    <p>We have leading class machinery, CNC Hass, conventional lathes, milling machines, EDM (Electrical Discharge Machining), sandblast cabins, tempering furnaces and more specialized equipment in order to carry out any project.</p>',

];

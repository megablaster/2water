<?php

return [

    'title' => 'Business Line',
    'promotion' => [
        'title' => 'Promotional Items',
        'text1' => 'National leader in manufacturing and design of promotional items.',
        'text2' => 'With more than 10 years of experience developing promotional items at 2W Plásticos, we firmly believe in innovation, in the Mexican industry, in good quality and commitment to our customers.',
        'text3' => 'Today, we are proudly the # 1 manufacturer of plastic promotional items in Mexico, we have a wide range of products and work with the main importers of the country, thus considerably reducing imports from China, offering high quality products, design and competitive prices.'
    ],
    'retail' => [
        'title' => 'Home and Retail',
        'text1' => 'Our household and consumer items are nationally recognized, we have a wide range of products for self-service stores, catalog sales, department stores and convenience stores, thus managing to enter millions of Mexican homes. Our retail product line is designed to make life easier and more comfortable for all users.',
        'text2' => 'Quality and design are two of our main engines for the development of this product line, seeking to simplify daily life inside and outside the home. We also have a wide range of colors and materials and decorating services to make it more attractive and fun for users.',
    ],
    'envase' => [
        'title' => 'Packaging',
        'text1' => 'We have a wide range of products, models and materials for a broad range of industries in need of plastic packaging such as Food, Cosmetics, Pharmaceutical, among many others.',
        'text2' => 'We work with a wide selection of plastic materials such as PET, HDPE, LDPE, PP, PS and other more specialized ones. We also offer a variety of colors and finishes to ensure a modern, useful and quality packaging.',
        'text3' => 'We offer products from our catalog ready to be manufactured, or we can design molds according to the specific needs of the customer in terms of shape, capacity, size and thread type, as well as any specific requirement. Likewise, we can assign specific production lines per project and adjust them to the needs and demands of each customer, this way, we ensure delivery times according to their needs and assign the productive capacity required for the project.',
    ],
    'export' => [
        'title' => 'Export',
        'text1' => 'At 2W Plásticos, we have worked hard since our inception to achieve international standards in all of our products. Thanks to this, today we export to more than 15 countries in the continent.',
        'text2' => 'Today, we export to more than 100 different products throughout the continent, from Argentina to the United States, passing through:',
    ],
    'certificate' => [
        'title' => 'Certifications',
        'text1' => 'All of our raw materials are 100% virgin with FDA (U.S. Food and Drug Administration) certifications, are BPA (Bisphenol A) free and comply with California Prop 65. All the products we manufacture are 100% recyclable.',
        'text2' => 'We have certifications from international organizations such as Bureau Veristas and Intertek for Social Responsibility, Quality, Manufacturing Processes and SQP, Good Manufacturing Practices (GMP), Failure Modes and Effects Analysis (FME).',
        'text3' => 'Social Responsibility',
        'text4' => 'Quality',
        'text5' => 'Manufacturing Processes and SQP',
        'text6' => 'Good Manufacturing Practices (GMP)',
        'text7' => 'Failure Modes and Effects Analysis (FME)',
        'text8' => 'Proud member of',
    ],
    'maquila' => [
        'title' => 'Maquila',
        'text1' => 'Given our vast experience in injection molding and blow molding, you can be sure that your molds are in the best hands, we have a broad production line for injection molding, injetion blow molding and extrusion blow molding. We can work with a wide range and types of molds adapting them to our processes.',
        'text2' => 'We offer highly competitive prices and guarantee the quality and durability of your products and molds.',
        'text3' => 'We have diverse machinery in process type and injection molding or blow molding capacity, as well as vast experience with a wide range of thermoplastics.',
        'text4' => 'We work nationally and internationally with various industries such as parts or components maquila, for diferent industries like:',
        'text5' => 'Furniture',
        'text6' => 'Home Aplliances',
        'text7' => 'Automotive',
        'text8' => 'Hardware',
        'text9' => 'Plumbing',
        'text10' => 'Electronics',
        'text11' => 'Electric',
        'text12' => 'Sports',
        'text13' => 'Security',
        'text14' => 'Among other industries...',
        'text15' => 'Injection Molding',
        'text16' => 'Injection molding machines ranging from 80 to 300 tons',
        'text17' => 'Injection Blow Molding',
        'text18' => 'Next generation single stage blow injection machines',
        'text19' => 'Extrusion Blow Molding',
        'text20' => 'Machines with a capacity of 17 up to 67 liquid ounces',
    ],
];

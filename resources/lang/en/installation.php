<?php

return [
    'title' => 'Facilities',
    'plant_title' => 'Production Plant',
    'plant_text' => '
                <h4>Injection Molding</h4>
                <p>Injection molding machines ranging from 80 to 300 tons</p>
                <h4>Injection Blow Molding</h4>
                <p>Next generation single stage injection blow molding machines</p>
                <h4>Extrusion Blow Molding</h4>
                <p>Machines with a capacity of 17 and up to 67 liquid ounces</p>
    ',
    'taller_title' => 'Mold Workshop',
    'taller_text' => 'Mold workshop dedicated to repair and manufacture molds, equipped with top of the line machinery, from Haas CNC Machines, up to heat treatment ovens for tempering, conventional lathes and milling machines, 3D printers and high performance tools.',
    'laboratory_title' => 'Quality Control Lab',
    'laboratory_text' => '
            <p>Provided with ultra-precise equipment, our laboratory complies with international standards for the evaluation and quality assurance in all the productive processes we handle, thus achieving minimum levels of rejection and ensuring product traceability from our raw material suppliers, to our customers’ warehouses.</p>
            <p>All of 2W Plásticos facilities have quality processes and social responsibility certifications, which ensures both the quality and safety of all of our products, as well as the safety of all of us who are part of the team.</p>
    '
];

<?php

return [

    'search' => 'Search',
    'search_product' => 'Product or key...',
    'results' => 'Search results',
    'keywords' => 'Search word',
    'search' => 'Search',
    'category' => 'Category',
    'main_menu' => 'Menu',
    'phrase' => 'Consult manufacturing minimums with your advisor.',
    'information' => 'Ask for information',
    'info_aditional' => 'Additional Information',
    'submit' => 'Send',
    'title' => 'Questions, information or complaints <span style="font-weight:700;">¡Contact us!</span>',
    'title_job' => 'Job Board',
    'direction' => 'Address',
    'sales' => 'Phone',
    'sell' => 'Sales',
    'menu' => [
        'home' => 'Home',
        'products' => 'Products',
        'business_line' => 'Business lines',
        'services' => 'Services',
        'about' => 'About',
        'contact' => 'Contact us',
        'job' => 'Job Board',
        'politica' => 'Claims policy',
        'catalog' => 'Catalog'
    ],
    'info' => [
        'name' => 'Name',
        'capacity' => 'Capacity',
        'description' => 'Description',
        'color' => 'Colors',
        'services' => 'Services',
        'microowave' => 'Temperature resistance',
        'measure' => 'Measurements',
    ],
    'form' => [
        'name' => 'Name',
        'company' => 'Company',
        'email' => 'Email',
        'phone' => 'Phone',
        'color' => '-- Select the color --',
        'quantity' => 'Quantity',
        'comments' => 'Comments',
        'cv' => 'Upload CV in PDF'
    ],
    'footer' => [
        'aviso' => 'Privacy Notice',
        'derechos' => 'Access Rights (Arco)',
        'revocacion' =>'Withdrawal of consent',
        'reservados' => 'All Rights Reserved'
    ]
];

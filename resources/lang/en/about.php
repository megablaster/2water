<?php
    
    return [
        'title' => 'About us',
        'title_history' => 'Our History',
        'text_history' => '
        <p>The company was born in 2007 to meet a growing need in the promotional items market in Mexico. A company with more than 10 years of experience transforming plastic, developing high quality innovative products that lead in the country’s promotional items market. We work with the most renowned promotional items importers and distributors, as well as mass consumption companies and with world famous catalog sales leading companies.</p>
        <p>2W Plásticos is committed to meeting the needs of each customer, regardless of the product type or market to which it is aimed, we always find a way to develop the right product at the right price in order to achieve the results that our customers are looking for.</p>
        <p>Today, we have top level facilities certified by international organizations, complying with regulations and standards focused on quality, processes and, of course, social responsibility. Likewise, we have high autonomy in our internal departments to be able to provide service, maintenance and repair to our machinery immediately, in order to meet the commercial commitments we acquire.</p>
        <p>In 2016, we started exporting products to the United States, in 2017 to Central America, and in 2018 our products reached South America. We also have a local mold manufacturing workshop, which allows us to offer tailor-made solutions and developments at a great speed.</p>',
        'certificate' => 'Certifications',
        'installation' => 'Facilities'
    ];
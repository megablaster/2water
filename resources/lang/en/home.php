<?php

return [

    'title' => 'The plastic products factory in Mexico',
    'catalog' => 'See our catalog',
    'promotional' => 'Promotional Items',
    'specials' => 'Special Developments',
    'video' => 'Watch <span>our video</span>',
    'video_button' => 'Show video',
    'about_title' => 'About <span>Us</span>',
    'about_text' => 'The company was born in 2007 to meet a growing need in the promotional items market in Mexico. A company with more than 10 years of experience transforming plastic, developing high quality innovative products that lead in the country’s promotional items market.',
    'read_more' => 'Read more',
    'installation_title' => 'Our <span>Facilities</span>',
    'installation_text' => 'Located south of Mexico City, our facilities consist of more than 20,000 sq. ft of production area, which works 24/7.',
];

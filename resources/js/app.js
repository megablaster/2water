require('./bootstrap');

$(document).ready(function(){

    $('#lang_change').on('change',function(){
        $('#form-lang').submit();
    });

    $('.link-lang').on('click', function(e){
        e.preventDefault();
        var lang = $(this).data('lang');

        $('#lang_refill').val(lang);
        $('#lang-form').submit();
    });

});

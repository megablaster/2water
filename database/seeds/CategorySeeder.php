<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Cocina y hogar',
            'description' => '-',
            'url' => 'cocina-y-hogar'
        ]);

        DB::table('categories')->insert([
            'name' => 'Vasos y termos',
            'description' => '-',
            'url' => 'vasos-y-termos'
        ]);

        DB::table('categories')->insert([
            'name' => 'Bebés y niños',
            'description' => '-',
            'url' => 'bebes-y-niños'
        ]);

        DB::table('categories')->insert([
            'name' => 'Recreación',
            'description' => '-',
            'url' => 'recreacion'
        ]);

        DB::table('categories')->insert([
            'name' => 'Cilindros',
            'description' => '-',
            'url' => 'cilindros'
        ]);

        DB::table('categories')->insert([
            'name' => 'Salud y seguridad',
            'description' => '-',
            'url' => 'salud-y-seguridad'
        ]);
    }
}

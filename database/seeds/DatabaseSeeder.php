<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Categories
        DB::table('categories')->insert([
            'name' => 'Cocina y hogar',
            'description' => '-',
            'url' => 'cocina-y-hogar'
        ]);

        DB::table('categories')->insert([
            'name' => 'Vasos y termos',
            'description' => '-',
            'url' => 'vasos-y-termos'
        ]);

        DB::table('categories')->insert([
            'name' => 'Bebés y niños',
            'description' => '-',
            'url' => 'bebes-y-ninos'
        ]);

        DB::table('categories')->insert([
            'name' => 'Recreación',
            'description' => '-',
            'url' => 'recreacion'
        ]);

        DB::table('categories')->insert([
            'name' => 'Cilindros',
            'description' => '-',
            'url' => 'cilindros'
        ]);

        DB::table('categories')->insert([
            'name' => 'Salud y seguridad',
            'description' => '-',
            'url' => 'salud-y-seguridad'
        ]);

        DB::table('categories')->insert([
            'name' => 'Restaurante y hotel',
            'description' => '-',
            'url' => 'restaurante-y-hotel'
        ]);

        DB::table('categories')->insert([
            'name' => 'Botellas',
            'description' => '-',
            'url' => 'botellas'
        ]);

        DB::table('categories')->insert([
            'name' => 'Tapas',
            'description' => '-',
            'url' => 'tapas'
        ]);

        //Colors
        DB::table('colors')->insert([
            'name' => 'Rojo',
            'color' => '#E74C3C',
        ]);

        DB::table('colors')->insert([
            'name' => 'Morado',
            'color' => '#884EA0',
        ]);

        DB::table('colors')->insert([
            'name' => 'Azul',
            'color' => '#2E86C1',
        ]);

        DB::table('colors')->insert([
            'name' => 'Verde',
            'color' => '#17A589',
        ]);

        DB::table('colors')->insert([
            'name' => 'Naranja',
            'color' => '#F39C12',
        ]);

        DB::table('colors')->insert([
            'name' => 'Negro',
            'color' => '#17202A',
        ]);

        DB::table('colors')->insert([
            'name' => 'Blanco',
            'color' => '#FDFEFE',
        ]);

        DB::table('colors')->insert([
            'name' => 'Amarillo',
            'color' => '#fad638',
        ]);

        DB::table('colors')->insert([
            'name' => 'Rosa',
            'color' => '#da3886',
        ]);

        //Products
        // DB::table('products')->insert([
        //     'name' => 'Contenedor de alimentos 500 ml',
        //     'description' => 'Contenedor rectangular para microondas (con válvula) Capacidad 500 ml Material PP',
        //     'sku' => 'Promo1',
        //     'category_id' => 1,
        //     'price' => 156,
        //     'url' => 'contenedor-de-alimentos-500',
        //     'capacity' => '500 ml / 17 oz',
        //     'services' => 'Serigrafía, Tampografía y Grabado Área de impresión.',
        //     'measure' => '15 x 9.4 x 5.7 cm'
        // ]);

        // DB::table('products')->insert([
        //     'name' => 'Contenedor de alimentos 750 ml',
        //     'description' => 'Contenedor rectangular para microondas (con válvula) Capacidad 750 ml Material PP',
        //     'sku' => 'Promo2',
        //     'category_id' => 1,
        //     'price' => 246,
        //     'url' => 'contenedor-de-alimentos-750',
        //     'capacity' => '750 ml /25 oz',
        //     'services' => 'Serigrafía, Tampografía y Grabado Área de impresión.',
        //     'measure' => '17.3 x 10.9 x 6.8 cm'
        // ]);

        // DB::table('products')->insert([
        //     'name' => 'Contenedor de alimentos 1,400 ml',
        //     'description' => 'Contenedor rectangular para microondas (con válvula) Capacidad 1,400 ml Material PP',
        //     'sku' => 'Promo3',
        //     'category_id' => 1,
        //     'price' => 357,
        //     'url' => 'contenedor-de-alimentos-1400',
        //     'capacity' => '1,400 ml / 47oz',
        //     'services' => 'Serigrafía, Tampografía y Grabado Área de impresión.',
        //     'measure' => '19.8 x 13.3. x 7.5 cm'
        // ]);

        // DB::table('products')->insert([
        //     'name' => 'Contenedor de alimentos 250 ml',
        //     'description' => 'Contenedor de alimentos cuadrado Capacidad 250 ml Material PP',
        //     'sku' => 'Promo4',
        //     'category_id' => 1,
        //     'price' => 167,
        //     'url' => 'contenedor-de-alimentos-250',
        //     'capacity' => '250 ml / 8.50 oz',
        //     'services' => 'Serigrafía, Tampografía y Grabado Área de impresión.',
        //     'measure' => '9.9 x 9.9 x 5.3 cm'
        // ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Contenedor de alimentos 500 ml',
            'description' => 'Contenedor rectangular para microondas (con válvula) Capacidad 500 ml Material PP',
            'sku' => 'Promo1',
            'category_id' => 1,
            'subcategory_id' => 1,
            'price' => 156,
            'url' => 'contenedor-de-alimentos-500',
            'capacity' => '500 ml / 17 oz',
            'services' => 'Serigrafía, Tampografía y Grabado Área de impresión.',
            'measure' => '15 x 9.4 x 5.7 cm'
        ]);

        DB::table('products')->insert([
            'name' => 'Contenedor de alimentos 750 ml',
            'description' => 'Contenedor rectangular para microondas (con válvula) Capacidad 750 ml Material PP',
            'sku' => 'Promo2',
            'category_id' => 1,
            'subcategory_id' => 1,
            'price' => 246,
            'url' => 'contenedor-de-alimentos-750',
            'capacity' => '750 ml /25 oz',
            'services' => 'Serigrafía, Tampografía y Grabado Área de impresión.',
            'measure' => '17.3 x 10.9 x 6.8 cm'
        ]);

        DB::table('products')->insert([
            'name' => 'Contenedor de alimentos 1,400 ml',
            'description' => 'Contenedor rectangular para microondas (con válvula) Capacidad 1,400 ml Material PP',
            'sku' => 'Promo3',
            'category_id' => 1,
            'subcategory_id' => 1,
            'price' => 357,
            'url' => 'contenedor-de-alimentos-1400',
            'capacity' => '1,400 ml / 47oz',
            'services' => 'Serigrafía, Tampografía y Grabado Área de impresión.',
            'measure' => '19.8 x 13.3. x 7.5 cm'
        ]);

        DB::table('products')->insert([
            'name' => 'Contenedor de alimentos 250 ml',
            'description' => 'Contenedor de alimentos cuadrado Capacidad 250 ml Material PP',
            'sku' => 'Promo4',
            'category_id' => 1,
            'subcategory_id' => 1,
            'price' => 167,
            'url' => 'contenedor-de-alimentos-250',
            'capacity' => '250 ml / 8.50 oz',
            'services' => 'Serigrafía, Tampografía y Grabado Área de impresión.',
            'measure' => '9.9 x 9.9 x 5.3 cm'
        ]);
    }
}

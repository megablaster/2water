<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->set('status',['on','off'])->default('on')->nullable();
            $table->string('name');
            $table->text('description');
            $table->string('sku')->nullable();
            $table->decimal('price','9',2)->nullable();
            $table->string('img')->default('default.png');
            $table->string('capacity')->nullable();
            $table->string('services')->nullable();
            $table->string('url')->nullable();
            $table->integer('views')->default(0);
            $table->set('microwave',['on','off'])->default('on');
            $table->string('measure')->nullable();
            $table->string('datasheet')->nullable();
            $table->unsignedBigInteger('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

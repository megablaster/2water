<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUsersTable extends  migration
{

    /**No h
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            //Fiels extra
            $table->string('img')->default('default.png');
            $table->string('last_name')->nullable();
            $table->string('last_name_mother')->nullable();

            $table->string('phone')->nullable();
            $table->text('address')->nullable();
            $table->string('state')->nullable();

            $table->date('birthday')->nullable();
            $table->set('gender',['male','female'])->default('male');
            $table->string('cp')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('img');
            $table->dropColumn('last_name');
            $table->dropColumn('last_name_mother');

            $table->dropColumn('phone');
            $table->dropColumn('address');
            $table->dropColumn('state');

            $table->dropColumn('birthday');
            $table->dropColumn('gender');
            $table->dropColumn('cp');
        });
    }
}

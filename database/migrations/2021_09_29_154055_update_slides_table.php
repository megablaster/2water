<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('slides');

        Schema::create('slides', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('img')->default('default.png');
            $table->string('url')->nullable();
            $table->set('select',['url','archive','empty'])->default('empty');
            $table->string('archive')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

$(document).ready(function(){

    //Add scroll navbar
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();

        if (scroll >= 120) {
            $("#navbar").addClass("fixed");
        } else {
            $("#navbar").removeClass("fixed");
        }
    });

    //Click mobile
    $('#click-mobile').on('click', function(){

        $('#navbar').toggleClass('margin');

    });

});
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\AssistantController;
use App\Http\Controllers\ConsultationController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ColorProductController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\SlideController;
use App\Http\Controllers\LeadController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\SystemController;
use App\Http\Controllers\ColorController;
use App\Lead;
use App\Order;
use App\Mail\Contact;
use App\Mail\Job;
use App\Mail\Order as Orders;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//FrontEnd
Route::get('/',[HomeController::class,'index'])->name('front.index');
Route::get('productos',[HomeController::class,'products'])->name('front.products');
Route::get('lineas-de-negocio',[HomeController::class,'business_line'])->name('front.business');
Route::get('servicios',[HomeController::class,'services'])->name('front.services');
Route::get('nosotros',[HomeController::class,'about'])->name('front.about');
Route::get('instalaciones',[HomeController::class,'installation'])->name('front.installation');
Route::get('contacto',[HomeController::class,'contact'])->name('front.contact');
Route::get('bolsa-de-trabajo',[HomeController::class,'job'])->name('front.job');
Route::get('busqueda',[HomeController::class,'search'])->name('front.search');
Route::get('productos/{category}/{url}',[HomeController::class,'single'])->name('front.single');
Route::get('categoria/{url}',[HomeController::class,'category'])->name('front.category');
Route::post('lang/change',[HomeController::class,'lang_change'])->name('lang.change');
Route::post('lang/change-inline',[HomeController::class,'lang_changeinline'])->name('lang.changeinline');
Route::get('catalogo-digital',[HomeController::class,'catalog'])->name('catalog.digital');

//Mailable
Route::get('/mailable', function () {
    $order = Order::find(3);
    return new Orders($order);
});

//Forms
Route::post('send_contact',[HomeController::class,'send_contact'])->name('send.contact');
Route::post('send_job',[HomeController::class,'send_job'])->name('send.job');
Route::post('send_order',[HomeController::class,'send_product'])->name('send.product');

Route::get('login', [LoginController::class,'showLoginForm'])->name('login');
Route::post('login', [LoginController::class,'login']);
Route::post('register', [RegisterController::class,'register']);

Route::get('password/forget',  function () {
	return view('pages.forgot-password');
})->name('password.forget');

Route::post('password/email', [ForgotPasswordController::class,'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}', [ResetPasswordController::class,'showResetForm'])->name('password.reset');
Route::post('password/reset', [ResetPasswordController::class,'reset'])->name('password.update');

Route::group(['middleware' => 'auth'], function(){

	// logout route
	Route::get('/logout', [LoginController::class,'logout']);
	Route::get('/clear-cache', [HomeController::class,'clearCache']);

	// dashboard route
	Route::get('dashboard',[AdminController::class,'index'])->name('dashboard');

	Route::get('storage/{img}',[UserController::class,'storage'])->name('get.image');

	//only those have manage_user permission will get access
	Route::group(['middleware' => 'can:manage_user'], function(){
		Route::get('/users', [UserController::class,'index']);
		Route::get('/user/get-list', [UserController::class,'getUserList']);
		Route::get('/user/create', [UserController::class,'create']);
		Route::post('/user/create', [UserController::class,'store'])->name('create-user');
		Route::get('/user/{id}', [UserController::class,'edit']);
		Route::post('/user/update', [UserController::class,'update']);
		Route::get('/user/delete/{id}', [UserController::class,'delete']);
	});

	//Manage role
	Route::group(['middleware' => 'can:manage_role|manage_user'], function(){
		Route::get('/roles', [RolesController::class,'index']);
		Route::get('/role/get-list', [RolesController::class,'getRoleList']);
		Route::post('/role/create', [RolesController::class,'create']);
		Route::get('/role/edit/{id}', [RolesController::class,'edit']);
		Route::post('/role/update', [RolesController::class,'update']);
		Route::get('/role/delete/{id}', [RolesController::class,'delete']);

	});

	//Manage
	Route::group(['middleware' => 'can:manage_permission|manage_user'], function(){

		Route::get('/permission', [PermissionController::class,'index']);
		Route::get('/permission/get-list', [PermissionController::class,'getPermissionList']);
		Route::post('/permission/create', [PermissionController::class,'create']);
		Route::get('/permission/update', [PermissionController::class,'update']);
		Route::get('/permission/delete/{id}', [PermissionController::class,'delete']);

	});

    //Products
    Route::group(['middleware' => 'can:manage_product'], function(){
        Route::get('/products', [ProductController::class,'index']);
        Route::get('/product/get-list', [ProductController::class,'getProductList']);
        Route::get('/product/create', [ProductController::class,'create']);
        Route::post('/product/create', [ProductController::class,'store'])->name('create-product');
        Route::get('/product/{id}', [ProductController::class,'edit']);
        Route::post('/product/update', [ProductController::class,'update']);
        Route::get('/product/delete/{id}', [ProductController::class,'delete']);
        Route::post('/product/import', [ProductController::class,'import'])->name('import.product');
    });

    //Colors
    Route::group(['middleware' => 'can:manage_product'], function(){

        Route::get('/colors', [ColorController::class,'index']);
        Route::get('/color/get-list', [ColorController::class,'getColorList']);
        Route::get('/color/created', [ColorController::class,'create']);
        Route::post('/color/created', [ColorController::class,'store'])->name('add.color');
        Route::get('/color/{id}', [ColorController::class,'edit']);
        Route::post('/color/update', [ColorController::class,'update']);
        Route::get('/color/deleted/{id}', [ColorController::class,'delete']);

    });

    Route::post('products/reorder', [ProductController::class,'reorder'])->name('product.reorder');

    //Leads
    Route::group(['middleware' => 'can:manage_product'], function(){

        Route::get('/leads', [LeadController::class,'index']);
        Route::get('/lead/get-list', [LeadController::class,'getLeadList']);
        Route::get('/lead/{id}', [LeadController::class,'show']);

    });

    //Orders
    Route::group(['middleware' => 'can:manage_product'], function(){

        Route::get('/orders', [OrderController::class,'index']);
        Route::get('/order/get-list', [OrderController::class,'getOrderList']);
        Route::get('/order/{id}', [OrderController::class,'show']);

    });

    //Leads
    Route::group(['middleware' => 'can:manage_product'], function(){

        Route::get('/systems', [SystemController::class,'index']);
        Route::get('/system/{id}', [SystemController::class,'edit'])->name('system.edit');
        Route::post('/system/update', [SystemController::class,'update']);

    });

    //Colors
    Route::group(['middleware' => 'can:manage_product'], function(){
    	Route::post('/color/create', [ColorProductController::class,'store'])->name('create-color');
        Route::post('/color/update', [ColorProductController::class,'update']);
        Route::get('/color/delete/{id}', [ColorProductController::class,'delete']);
    });

    //Sliders
    Route::group(['middleware' => 'can:manage_slider'], function(){
    	Route::get('/sliders', [SlideController::class,'index']);
        Route::get('/slider/get-list', [SlideController::class,'getSliderList']);
        Route::get('/slider/create', [SlideController::class,'create']);
    	Route::post('/slider/create', [SlideController::class,'store'])->name('create-slider');
    	Route::get('/slider/{id}', [SlideController::class,'edit']);
        Route::post('/slider/update', [SlideController::class,'update']);
        Route::get('/slider/delete/{id}', [SlideController::class,'delete']);
    });

	// get permissions
	Route::get('get-role-permissions-badge', [PermissionController::class,'getPermissionBadgeByRole']);

});


//Route::get('/register', function () { return view('pages.register'); });
